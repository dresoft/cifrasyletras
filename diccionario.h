#ifndef DICCIONARIO_H
#define DICCIONARIO_H

#include <QStringList>
#include <QMap>
#include <QObject>

class Diccionario : public QObject {
Q_OBJECT

public:

    enum Idiomas {
        ESP,
        ENG
    };

    static Diccionario *getInstance();
    bool cambiarIdioma(Idiomas idioma);
    bool test();

    QString buscarPalabraMasLarga(QString texto);
    QStringList buscarTodasLasPalabrasPosibles(QString texto);
private:

    static Diccionario *instance_;
    //Listas con las palabras existentes de X letras
    //2 listas, una con las palabras reales y otra con las mismas palabras con sus letras ordenadas alfabeticamente
    QPair<QStringList,QStringList> palabrasDe9_;
    QPair<QStringList,QStringList> palabrasDe8_;
    QPair<QStringList,QStringList> palabrasDe7_;
    QPair<QStringList,QStringList> palabrasDe6_;
    QPair<QStringList,QStringList> palabrasDe5_;
    QPair<QStringList,QStringList> palabrasDe4_;
    QPair<QStringList,QStringList> palabrasDe3_;
    QPair<QStringList,QStringList> palabrasDe2_;

    Diccionario();
    Idiomas idiomaActual_;
    //Variable que almacena un mapa con la letra y la lista de palabras que comienzan con esa letra
    QMap <QChar,QStringList> diccionario_;

    bool cargarLetra(QChar letra);
    bool cargarDiccionario(Idiomas);
    QString ordenaLetrasPalabra(QString palabra);

    bool cargarPalabrasDe9DelDiccionario();
    bool cargarPalabrasDe8DelDiccionario();
    bool cargarPalabrasDe7DelDiccionario();
    bool cargarPalabrasDe6DelDiccionario();
    bool cargarPalabrasDe5DelDiccionario();
    bool cargarPalabrasDe4DelDiccionario();
    bool cargarPalabrasDe3DelDiccionario();
    bool cargarPalabrasDe2DelDiccionario();

public slots:
    bool existePalabra(QString palabra);

signals:
    void letraCargada(QChar letra);    
};
#endif // DICCIONARIO_H
