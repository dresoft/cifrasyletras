#include <QApplication>
#include <QQmlApplicationEngine>
#include <QTextCodec>
#include <QGuiApplication>
#include <QQmlContext>
#include <QDebug>

#include "hilocargadiccionario.h"
#include "receiver.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

    //cargamos los diccionarios y las palabras en un hilo aparte
    HiloCargaDiccionario *hilo = new HiloCargaDiccionario();
    hilo->start();

    //Receiver es una clase que se encarga de gestionar las señales llegadas de qml
    Receiver receiver;
    qDebug() << "CARGAMOS LOS RECORDS" << receiver.setRecords();
    QQmlContext* ctx = engine.rootContext();
    //Introducimos el receiver como objeto
    ctx->setContextProperty("receiver", &receiver);

    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));

    return app.exec();
}
