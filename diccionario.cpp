#include <QFile>
#include <QTextStream>
#include <QTextCodec>
#include <QDebug>
#include <QTime>

#include "diccionario.h"

Diccionario* Diccionario::instance_ = 0;
Diccionario::Diccionario()
{

    idiomaActual_ = ESP;
    QTime t;
    t.start();
    bool resultado = cargarDiccionario(idiomaActual_);
    if (resultado)
        qDebug() << "== Diccionario Español cargado en " << t.elapsed() << " ms ==";
    else
        qDebug() << "== Diccionario Español cargado con problemas ==";

    //cargamos en memoria unas listas con las palabras del
    //diccionario que tengan 9,8,7,6 y 5 letras respectivamente
    cargarPalabrasDe9DelDiccionario();
    cargarPalabrasDe8DelDiccionario();
    cargarPalabrasDe7DelDiccionario();
    cargarPalabrasDe6DelDiccionario();
    cargarPalabrasDe5DelDiccionario();
    cargarPalabrasDe4DelDiccionario();
    cargarPalabrasDe3DelDiccionario();
    cargarPalabrasDe2DelDiccionario();

    //test();
}

Diccionario *Diccionario::getInstance()
{
    if (instance_ == NULL)
        instance_ = new Diccionario();

    return instance_;
}

//Carga todas las palabras del idioma seleccionado en un QMap con clave QChar con
//la inicial y valor QStringList con todas las palabras que comienzan por esa letra
bool Diccionario::cargarDiccionario(Idiomas idioma)
{
    QString abcEsp = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
    QString abcEng = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    QString abc;
    bool salida = true;

    switch (idioma) {
    case ESP:
        abc = abcEsp;
        break;
    case ENG:
        abc = abcEng;
        break;
    default:
        abc = abcEsp;
        break;
    }
    foreach (QChar a, abc) {
      if (!cargarLetra(a))
          salida = false;
    }

    return salida;
}

//Carga las palabras de cada letra en un QStringList con el nombre de la letra
bool Diccionario::cargarLetra(QChar letra)
{
    QString nombreDic = ":/dic_es/diccionario/";
    QString fileName;
    QString let(letra);
    if (let == "Ñ")
    {
        fileName = nombreDic.append("NH");
    }
    else
        fileName = nombreDic.append(letra);

    QFile file(fileName);
    qDebug() << " file.exists(" << fileName << ") = " << file.exists();
    QStringList lista;
    QString linea;
    bool salida = false;
    try
    {
        if (file.open(QFile::ReadOnly))
        {
            QTextStream stream(&file);
            do {
                linea = stream.readLine();
                lista.append(linea);
            } while (!linea.isNull());
            file.close();

            salida = true;
//            qDebug() << QString("Palabras con la letra %1 cargadas en el diccionario").arg(letra);
        }
        else
        {
            qDebug() << QString("No se ha podido abrir el archivo con las palabras que empiezan por %1").arg(letra);
        }
    }
    catch(...)
    {
        qDebug() << QString("Excepcion cargando las palabras que empiezan por %1").arg(letra);
    }

    diccionario_.insert(letra,lista); //IDEA - HACER un diccionario con un QMap template de QChar y QStringList
    return salida;
}

QString Diccionario::ordenaLetrasPalabra(QString palabra)
{
    std::string letras = palabra.toStdString();
    std::sort(letras.begin(),letras.end());
    return QString::fromStdString(letras);
}

bool Diccionario::cambiarIdioma(Idiomas idioma)
{
    Q_UNUSED(idioma);
    idiomaActual_ = idioma;
    diccionario_.clear();
    return (cargarDiccionario(idioma));
}

//Devueve true si la palabra esta en el diccionario y false en caso contrario
bool Diccionario::existePalabra(QString palabra)
{
    if (palabra == "")
        return false;

    QTime time;
    time.start();
    // Ojo!! Hemos introducido las iniciales en mayusculas
    QChar inicial = palabra.at(0).toUpper();
    QStringList lista = diccionario_[inicial]; //cogemos la lista con las palabras reales, no con las letras ordenadas alfabeticamente, la primera

    bool salida = lista.contains(palabra,Qt::CaseInsensitive);
    //qDebug() << "Diccionario::existePalabra(" << palabra << ") = " << salida << " (" << time.elapsed() << "ms)";
    return salida;
}

bool Diccionario::test()
{
    QTime t;
    t.start();

    qDebug() << "============================================";
    qDebug() << "==========   TEST DICCIONARIO    ===========";
    qDebug() << "============================================";
    qDebug() << "============================================";
    qDebug() << "== Nº Letras abecedario: " << diccionario_.count();
    qDebug() << "============================================";
    qDebug() << "== PALABRA ENCONTRADA CON abcdefghi: " << buscarPalabraMasLarga("abcdefghi");
    qDebug() << "Tiempo en buscar = " << t.restart() << " ms";
    qDebug() << "== TODAS PALABRAS CON abcdefghi: " << buscarTodasLasPalabrasPosibles("abcdefghi");
    qDebug() << "Tiempo en buscar = " << t.restart() << " ms ===";
    qDebug() << "== PALABRA ENCONTRADA CON retrocdee: " << buscarPalabraMasLarga("retrocdee");
    qDebug() << "Tiempo en buscar = " << t.restart() << " ms ===";
    qDebug() << "== TODAS PALABRAS CON retrocdee: " << buscarTodasLasPalabrasPosibles("retrocdee");
    qDebug() << "Tiempo en buscar = " << t.restart() << " ms ===";
    qDebug() << "PALABRA ENCONTRADA CON prealedea: " << buscarPalabraMasLarga("prealedea");
    qDebug() << "Tiempo en buscar = " << t.restart() << " ms ===";
    qDebug() << "== TODAS PALABRAS CON  prealedea: " << buscarTodasLasPalabrasPosibles("prealedea");
    qDebug() << "Tiempo en buscar = " << t.restart() << " ms ===";
    qDebug() << "== PALABRA ENCONTRADA CON wxrtghjyz: " << buscarPalabraMasLarga("wxrtghjyz");
    qDebug() << "Tiempo en buscar = " << t.restart() << " ms ===";
    qDebug() << "== TODAS PALABRAS CON wxrtghjyz: " << buscarTodasLasPalabrasPosibles("wxrtghjyz");
    qDebug() << "Tiempo en buscar = " << t.elapsed() << " ms ===";
    qDebug() << "============================================";
    qDebug() << "== Duracion  de la prueba: " << t.elapsed() << "ms ===";
    qDebug() << "============================================";
    return true;
}

bool Diccionario::cargarPalabrasDe9DelDiccionario()
{
    QStringList listaTemp;
    QStringList listaDe9;

    if (palabrasDe9_.first.count() != 0)
    {
        qDebug() << "Lista de palabras de 9 ya cargada";
        return false;
    }

    QTime time;
    time.start();

    //Recorremos el diccionario y creamos una lista con todas las palabras de 9 que hay en el
    QMap<QChar,QStringList>::iterator it;
    for (it = diccionario_.begin(); it != diccionario_.end(); ++it)
    {
        listaTemp = it.value();

        for (int i=0;i<listaTemp.count();i++)
        {
            if (listaTemp.at(i).length() == 9)
              listaDe9.append(listaTemp.at(i));
        }
    }

    QStringList listaOrdenada;
    for (int i=0;i<listaDe9.length();i++)
    {
        listaOrdenada.append(ordenaLetrasPalabra(listaDe9.at(i)));
    }

    palabrasDe9_ = QPair<QStringList,QStringList> (listaDe9,listaOrdenada);
    qDebug() << "== Duracion  de la carga de las " << listaDe9.count() << " palabras de 9: " << time.elapsed() << "ms ====";
    return true;
}

bool Diccionario::cargarPalabrasDe8DelDiccionario()
{
    QStringList listaTemp;
    QStringList listaDe8;

    if (palabrasDe8_.first.count() != 0)
    {
        qDebug() << "Lista de palabras de 8 ya cargada";
        return false;
    }

    QTime time;
    time.start();

    //Recorremos el diccionario y creamos una lista con todas las palabras de 8 que hay en el
    QMap<QChar,QStringList>::iterator it;
    for (it = diccionario_.begin(); it != diccionario_.end(); ++it)
    {
        listaTemp = it.value();

        for (int i=0;i<listaTemp.count();i++)
        {
            if (listaTemp.at(i).length() == 8)
              listaDe8.append(listaTemp.at(i));
        }
    }

    QStringList listaOrdenada;
    for (int i=0;i<listaDe8.length();i++)
    {
        listaOrdenada.append(ordenaLetrasPalabra(listaDe8.at(i)));
    }

    palabrasDe8_ = QPair<QStringList,QStringList> (listaDe8,listaOrdenada);
    qDebug() << "== Duracion  de la carga de las " << listaDe8.count() << " palabras de 8: " << time.elapsed() << "ms ====";
    return true;
}

bool Diccionario::cargarPalabrasDe7DelDiccionario()
{
    QStringList listaTemp;
    QStringList listaDe7;

    if (palabrasDe7_.first.count() != 0)
    {
        qDebug() << "Lista de palabras de 7 ya cargada";
        return false;
    }

    QTime time;
    time.start();

    //Recorremos el diccionario y creamos una lista con todas las palabras de 7 que hay en el
    QMap<QChar,QStringList>::iterator it;
    for (it = diccionario_.begin(); it != diccionario_.end(); ++it)
    {
        listaTemp = it.value();

        for (int i=0;i<listaTemp.count();i++)
        {
            if (listaTemp.at(i).length() == 7)
              listaDe7.append(listaTemp.at(i));
        }
    }

    QStringList listaOrdenada;
    for (int i=0;i<listaDe7.length();i++)
    {
        listaOrdenada.append(ordenaLetrasPalabra(listaDe7.at(i)));
    }

    palabrasDe7_ = QPair<QStringList,QStringList> (listaDe7,listaOrdenada);
    qDebug() << "== Duracion  de la carga de las " << listaDe7.count() << " palabras de 7: " << time.elapsed() << "ms ====";
    return true;
}

bool Diccionario::cargarPalabrasDe6DelDiccionario()
{
    QStringList listaTemp;
    QStringList listaDe6;

    if (palabrasDe6_.first.count() != 0)
    {
        qDebug() << "Lista de palabras de 6 ya cargada";
        return false;
    }

    QTime time;
    time.start();

    //Recorremos el diccionario y creamos una lista con todas las palabras de 6 que hay en el
    QMap<QChar,QStringList>::iterator it;
    for (it = diccionario_.begin(); it != diccionario_.end(); ++it)
    {
        listaTemp = it.value();

        for (int i=0;i<listaTemp.count();i++)
        {
            if (listaTemp.at(i).length() == 6)
              listaDe6.append(listaTemp.at(i));
        }
    }

    QStringList listaOrdenada;
    for (int i=0;i<listaDe6.length();i++)
    {
        listaOrdenada.append(ordenaLetrasPalabra(listaDe6.at(i)));
    }

    palabrasDe6_ = QPair<QStringList,QStringList> (listaDe6,listaOrdenada);
    qDebug() << "== Duracion  de la carga de las " << listaDe6.count() << " palabras de 6: " << time.elapsed() << "ms ====";
    return true;
}

bool Diccionario::cargarPalabrasDe5DelDiccionario()
{
    QStringList listaTemp;
    QStringList listaDe5;

    if (palabrasDe5_.first.count() != 0)
    {
        qDebug() << "Lista de palabras de 5 ya cargada";
        return false;
    }

    QTime time;
    time.start();

    //Recorremos el diccionario y creamos una lista con todas las palabras de 5 que hay en el
    QMap<QChar,QStringList>::iterator it;
    for (it = diccionario_.begin(); it != diccionario_.end(); ++it)
    {
        listaTemp = it.value();

        for (int i=0;i<listaTemp.count();i++)
        {
            if (listaTemp.at(i).length() == 5)
              listaDe5.append(listaTemp.at(i));
        }
    }

    QStringList listaOrdenada;
    for (int i=0;i<listaDe5.length();i++)
    {
        listaOrdenada.append(ordenaLetrasPalabra(listaDe5.at(i)));
    }

    palabrasDe5_ = QPair<QStringList,QStringList> (listaDe5,listaOrdenada);
    qDebug() << "== Duracion  de la carga de las " << listaDe5.count() << " palabras de 5: " << time.elapsed() << "ms ====";
    return true;
}

bool Diccionario::cargarPalabrasDe4DelDiccionario()
{
    QStringList listaTemp;
    QStringList listaDe4;

    if (palabrasDe4_.first.count() != 0)
    {
        qDebug() << "Lista de palabras de 4 ya cargada";
        return false;
    }

    QTime time;
    time.start();

    //Recorremos el diccionario y creamos una lista con todas las palabras de 4 que hay en el
    QMap<QChar,QStringList>::iterator it;
    for (it = diccionario_.begin(); it != diccionario_.end(); ++it)
    {
        listaTemp = it.value();

        for (int i=0;i<listaTemp.count();i++)
        {
            if (listaTemp.at(i).length() == 4)
              listaDe4.append(listaTemp.at(i));
        }
    }

    QStringList listaOrdenada;
    for (int i=0;i<listaDe4.length();i++)
    {
        listaOrdenada.append(ordenaLetrasPalabra(listaDe4.at(i)));
    }

    palabrasDe4_ = QPair<QStringList,QStringList> (listaDe4,listaOrdenada);
    qDebug() << "== Duracion  de la carga de las " << listaDe4.count() << " palabras de 4: " << time.elapsed() << "ms ====";
    return true;
}

bool Diccionario::cargarPalabrasDe3DelDiccionario()
{
    QStringList listaTemp;
    QStringList listaDe3;

    if (palabrasDe3_.first.count() != 0)
    {
        qDebug() << "Lista de palabras de 3 ya cargada";
        return false;
    }

    QTime time;
    time.start();

    //Recorremos el diccionario y creamos una lista con todas las palabras de 3 que hay en el
    QMap<QChar,QStringList>::iterator it;
    for (it = diccionario_.begin(); it != diccionario_.end(); ++it)
    {
        listaTemp = it.value();

        for (int i=0;i<listaTemp.count();i++)
        {
            if (listaTemp.at(i).length() == 3)
              listaDe3.append(listaTemp.at(i));
        }
    }

    QStringList listaOrdenada;
    for (int i=0;i<listaDe3.length();i++)
    {
        listaOrdenada.append(ordenaLetrasPalabra(listaDe3.at(i)));
    }

    palabrasDe3_ = QPair<QStringList,QStringList> (listaDe3,listaOrdenada);
    qDebug() << "== Duracion  de la carga de las " << listaDe3.count() << " palabras de 3: " << time.elapsed() << "ms ====";
    return true;
}

bool Diccionario::cargarPalabrasDe2DelDiccionario()
{
    QStringList listaTemp;
    QStringList listaDe2;

    if (palabrasDe2_.first.count() != 0)
    {
        qDebug() << "Lista de palabras de 2 ya cargada";
        return false;
    }

    QTime time;
    time.start();

    //Recorremos el diccionario y creamos una lista con todas las palabras de 2 que hay en el
    QMap<QChar,QStringList>::iterator it;
    for (it = diccionario_.begin(); it != diccionario_.end(); ++it)
    {
        listaTemp = it.value();

        for (int i=0;i<listaTemp.count();i++)
        {
            if (listaTemp.at(i).length() == 2)
              listaDe2.append(listaTemp.at(i));
        }
    }

    QStringList listaOrdenada;
    for (int i=0;i<listaDe2.length();i++)
    {
        listaOrdenada.append(ordenaLetrasPalabra(listaDe2.at(i)));
    }

    palabrasDe2_ = QPair<QStringList,QStringList> (listaDe2,listaOrdenada);
    qDebug() << "== Duracion  de la carga de las " << listaDe2.count() << " palabras de 2: " << time.elapsed() << "ms ====";
    return true;
}

//-- FUNCIONES DE BUSQUEDA --------------------------------------------------------------------------

//Esta funcion devuelve un lista todas las palabras que existen en el
//diccionario y que se pueden formar con las letras de texto
QStringList Diccionario::buscarTodasLasPalabrasPosibles(QString texto)
{
    //Ordenamos las letras
    const QString letras(ordenaLetrasPalabra(texto));
    QStringList salida;
    QStringList palabrasRevisadas;
    int cont9 = 0; //Contador con el numero de palabras de 9 letras encontradas
    int cont8 = 0; //Contador con el numero de palabras de 8 letras encontradas
    int cont7 = 0; //Contador con el numero de palabras de 7 letras encontradas
    int cont6 = 0; //Contador con el numero de palabras de 6 letras encontradas
    int cont5 = 0; //Contador con el numero de palabras de 5 letras encontradas
    int cont4 = 0; //Contador con el numero de palabras de 4 letras encontradas
    int cont3 = 0; //Contador con el numero de palabras de 3 letras encontradas
    int cont2 = 0; //Contador con el numero de palabras de 3 letras encontradas

    QStringList listaTempReales;
    QStringList listaTempOrdenadas;

    //-- Comprobamos si hay alguna palabra de 9 letras ---------------------------------

    listaTempReales = palabrasDe9_.first;
    listaTempOrdenadas = palabrasDe9_.second;

    //qDebug() << " BUSCAMOS AHORA LAS PALABRAS DE 9 CON LAS LETRAS: " <<letras;
    while (listaTempOrdenadas.contains(letras))
    {
        int index = listaTempOrdenadas.indexOf(letras);
        //Anadimos la letra encontrada a la lista de salida
        QString pal = listaTempReales.at(index);
        //No metemos palabras repetidas
        if (!salida.contains(pal))
        {
            cont9++;
            salida << pal;
        }
        listaTempOrdenadas.removeAt(index);
        listaTempReales.removeAt(index);
    }

    //-- Comprobamos si hay alguna de 8 -----------------------------------------------

    listaTempReales = palabrasDe8_.first;
    listaTempOrdenadas = palabrasDe8_.second;
    for (int i=0;i<letras.count();i++)
    {
        QString auxLetras;
        auxLetras = letras;
        auxLetras.remove(i,1);
        if (!palabrasRevisadas.contains(auxLetras))
        {
            //qDebug() << " BUSCAMOS AHORA LAS PALABRAS DE 8 CON LAS LETRAS: " <<auxLetras;
            while (listaTempOrdenadas.contains(auxLetras))
            {
                int index = listaTempOrdenadas.indexOf(auxLetras);
                //Anadimos la letra encontrada a la lista de salida
                QString pal = listaTempReales.at(index);
                //No metemos palabras repetidas
                if (!salida.contains(pal))
                {
                    cont8++;
                    salida << pal;
                }
                listaTempOrdenadas.removeAt(index);
                listaTempReales.removeAt(index);
            }
            palabrasRevisadas << auxLetras;
        }
    }
    //-- Comprobamos si hay alguna de 7 -----------------------------------------------

    listaTempReales = palabrasDe7_.first;
    listaTempOrdenadas = palabrasDe7_.second;
    palabrasRevisadas.clear();
    for (int i=0;i<letras.count();i++)
    {
        QString auxLetras;
        auxLetras = letras;
        auxLetras.remove(i,1);
        for (int j=0;j<auxLetras.count();j++)
        {
            QString auxLetras2;
            auxLetras2 = auxLetras;
            auxLetras2.remove(j,1);
            if (!palabrasRevisadas.contains(auxLetras2))
            {
              //qDebug() << " BUSCAMOS AHORA LAS PALABRAS DE 7 CON LAS LETRAS: " <<auxLetras2;
                while (listaTempOrdenadas.contains(auxLetras2))
                {
                    int index = listaTempOrdenadas.indexOf(auxLetras2);
                    //Anadimos la letra encontrada a la lista de salida
                    QString pal = listaTempReales.at(index);
                    //No metemos palabras repetidas
                    if (!salida.contains(pal))
                    {
                        cont7++;
                        salida << pal;
                    }
                    listaTempOrdenadas.removeAt(index);
                    listaTempReales.removeAt(index);
                }
                palabrasRevisadas << auxLetras2;
            }
        }
    }

    //-- Comprobamos si hay alguna de 6 -----------------------------------------------

    listaTempReales = palabrasDe6_.first;
    listaTempOrdenadas = palabrasDe6_.second;
    palabrasRevisadas.clear();
    for (int m=0;m<letras.count();m++)
    {
        QString auxLetras;
        auxLetras = letras;
        auxLetras.remove(m,1);
        for (int i=0;i<auxLetras.count();i++)
        {
            QString auxLetras2;
            auxLetras2 = auxLetras;
            auxLetras2.remove(i,1);
            for (int j=0;j<auxLetras2.count();j++)
            {
                QString auxLetras3;
                auxLetras3 = auxLetras2;
                auxLetras3.remove(j,1);
                if (!palabrasRevisadas.contains(auxLetras3))
                {
                    //qDebug() << " BUSCAMOS AHORA LAS PALABRAS DE 6 CON LAS LETRAS: " <<auxLetras3;
                    while (listaTempOrdenadas.contains(auxLetras3))
                    {
                        int index = listaTempOrdenadas.indexOf(auxLetras3);
                        //Anadimos la letra encontrada a la lista de salida
                        QString pal = listaTempReales.at(index);
                        //No metemos palabras repetidas
                        if (!salida.contains(pal))
                        {
                            cont6++;
                            salida << pal;
                        }
                        listaTempOrdenadas.removeAt(index);
                        listaTempReales.removeAt(index);
                    }
                    palabrasRevisadas << auxLetras3;
                }            
            }
        }
    }

    //-- Comprobamos si hay alguna de 5 -----------------------------------------------

    listaTempReales = palabrasDe5_.first;
    listaTempOrdenadas = palabrasDe5_.second;
    palabrasRevisadas.clear();
    for (int l=0;l<letras.count();l++)
    {
        QString auxLetras;
        auxLetras = letras;
        auxLetras.remove(l,1);
        for (int m=0;m<auxLetras.count();m++)
        {
            QString auxLetras2;
            auxLetras2 = auxLetras;
            auxLetras2.remove(m,1);
            for (int i=0;i<auxLetras2.count();i++)
            {
                QString auxLetras3;
                auxLetras3 = auxLetras2;
                auxLetras3.remove(i,1);
                for (int j=0;j<auxLetras3.count();j++)
                {
                    QString auxLetras4;
                    auxLetras4 = auxLetras3;
                    auxLetras4.remove(j,1);
                    if (!palabrasRevisadas.contains(auxLetras4))
                    {
                        //qDebug() << " BUSCAMOS AHORA LAS PALABRAS DE 5 CON LAS LETRAS: " <<auxLetras4;
                        while (listaTempOrdenadas.contains(auxLetras4))
                        {
                            int index = listaTempOrdenadas.indexOf(auxLetras4);
                            //Anadimos la letra encontrada a la lista de salida
                            QString pal = listaTempReales.at(index);
                            //No metemos palabras repetidas
                            if (!salida.contains(pal))
                            {
                                cont5++;
                                salida << pal;
                            }
                            listaTempOrdenadas.removeAt(index);
                            listaTempReales.removeAt(index);
                        }
                        palabrasRevisadas << auxLetras4;
                    }
                }
            }
        }
    }

    //-- Comprobamos si hay alguna de 4 -----------------------------------------------

    listaTempReales = palabrasDe4_.first;
    listaTempOrdenadas = palabrasDe4_.second;
    palabrasRevisadas.clear();
    for (int l=0;l<letras.count();l++)
    {
        QString auxLetras;
        auxLetras = letras;
        auxLetras.remove(l,1);
        for (int m=0;m<auxLetras.count();m++)
        {
            QString auxLetras2;
            auxLetras2 = auxLetras;
            auxLetras2.remove(m,1);
            for (int i=0;i<auxLetras2.count();i++)
            {
                QString auxLetras3;
                auxLetras3 = auxLetras2;
                auxLetras3.remove(i,1);
                for (int j=0;j<auxLetras3.count();j++)
                {
                    QString auxLetras4;
                    auxLetras4 = auxLetras3;
                    auxLetras4.remove(j,1);
                    for (int k=0;k<auxLetras4.count();k++)
                    {
                        QString auxLetras5;
                        auxLetras5 = auxLetras4;
                        auxLetras5.remove(k,1);
                        if (!palabrasRevisadas.contains(auxLetras5))
                        {
                            //qDebug() << " BUSCAMOS AHORA LAS PALABRAS DE 4 CON LAS LETRAS: " <<auxLetras5;
                            while (listaTempOrdenadas.contains(auxLetras5))
                            {
                                int index = listaTempOrdenadas.indexOf(auxLetras5);
                                //Anadimos la letra encontrada a la lista de salida
                                QString pal = listaTempReales.at(index);
                                //No metemos palabras repetidas
                                if (!salida.contains(pal))
                                {
                                    cont4++;
                                    salida << pal;
                                }
                                listaTempOrdenadas.removeAt(index);
                                listaTempReales.removeAt(index);
                            }
                            palabrasRevisadas << auxLetras5;
                        }
                    }
                }
            }
        }
    }

    //-- Comprobamos si hay alguna de 3 -----------------------------------------------

    listaTempReales = palabrasDe3_.first;
    listaTempOrdenadas = palabrasDe3_.second;
    palabrasRevisadas.clear();
    for (int l=0;l<letras.count();l++)
    {
        QString auxLetras;
        auxLetras = letras;
        auxLetras.remove(l,1);
        for (int m=0;m<auxLetras.count();m++)
        {
            QString auxLetras2;
            auxLetras2 = auxLetras;
            auxLetras2.remove(m,1);
            for (int i=0;i<auxLetras2.count();i++)
            {
                QString auxLetras3;
                auxLetras3 = auxLetras2;
                auxLetras3.remove(i,1);
                for (int j=0;j<auxLetras3.count();j++)
                {
                    QString auxLetras4;
                    auxLetras4 = auxLetras3;
                    auxLetras4.remove(j,1);
                    for (int k=0;k<auxLetras4.count();k++)
                    {
                        QString auxLetras5;
                        auxLetras5 = auxLetras4;
                        auxLetras5.remove(k,1);
                        for (int a=0;a<auxLetras5.count();a++)
                        {
                            QString auxLetras6;
                            auxLetras6 = auxLetras5;
                            auxLetras6.remove(a,1);
                            if (!palabrasRevisadas.contains(auxLetras6))
                            {
                                //qDebug() << " BUSCAMOS AHORA LAS PALABRAS DE 2 CON LAS LETRAS: " <<auxLetras6;
                                while (listaTempOrdenadas.contains(auxLetras6))
                                {
                                    int index = listaTempOrdenadas.indexOf(auxLetras6);
                                    //Anadimos la letra encontrada a la lista de salida
                                    QString pal = listaTempReales.at(index);
                                    //No metemos palabras repetidas
                                    if (!salida.contains(pal))
                                    {
                                        cont3++;
                                        salida << pal;
                                    }
                                    listaTempOrdenadas.removeAt(index);
                                    listaTempReales.removeAt(index);
                                }
                                palabrasRevisadas << auxLetras6;
                            }
                        }
                    }
                }
            }
        }
    }

    //-- Comprobamos si hay alguna de 2 -----------------------------------------------

    listaTempReales = palabrasDe2_.first;
    listaTempOrdenadas = palabrasDe2_.second;
    palabrasRevisadas.clear();
    for (int l=0;l<letras.count();l++)
    {
        QString auxLetras;
        auxLetras = letras;
        auxLetras.remove(l,1);
        for (int m=0;m<auxLetras.count();m++)
        {
            QString auxLetras2;
            auxLetras2 = auxLetras;
            auxLetras2.remove(m,1);
            for (int i=0;i<auxLetras2.count();i++)
            {
                QString auxLetras3;
                auxLetras3 = auxLetras2;
                auxLetras3.remove(i,1);
                for (int j=0;j<auxLetras3.count();j++)
                {
                    QString auxLetras4;
                    auxLetras4 = auxLetras3;
                    auxLetras4.remove(j,1);
                    for (int k=0;k<auxLetras4.count();k++)
                    {
                        QString auxLetras5;
                        auxLetras5 = auxLetras4;
                        auxLetras5.remove(k,1);
                        for (int a=0;a<auxLetras5.count();a++)
                        {
                            QString auxLetras6;
                            auxLetras6 = auxLetras5;
                            auxLetras6.remove(a,1);
                            for (int b=0;b<auxLetras6.count();b++)
                            {
                                QString auxLetras7;
                                auxLetras7 = auxLetras6;
                                auxLetras7.remove(b,1);
                                if (!palabrasRevisadas.contains(auxLetras7))
                                {
                                    //qDebug() << " BUSCAMOS AHORA LAS PALABRAS DE 2 CON LAS LETRAS: " <<auxLetras7;
                                    while (listaTempOrdenadas.contains(auxLetras7))
                                    {
                                        int index = listaTempOrdenadas.indexOf(auxLetras7);
                                        //Anadimos la letra encontrada a la lista de salida
                                        QString pal = listaTempReales.at(index);
                                        //No metemos palabras repetidas
                                        if (!salida.contains(pal))
                                        {
                                            cont2++;
                                            salida << pal;
                                        }
                                        listaTempOrdenadas.removeAt(index);
                                        listaTempReales.removeAt(index);
                                    }
                                    palabrasRevisadas << auxLetras7;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    qDebug() << "Palabras de 9 letras encontradas: " << cont9;
    qDebug() << "Palabras de 8 letras encontradas: " << cont8;
    qDebug() << "Palabras de 7 letras encontradas: " << cont7;
    qDebug() << "Palabras de 6 letras encontradas: " << cont6;
    qDebug() << "Palabras de 5 letras encontradas: " << cont5;
    qDebug() << "Palabras de 4 letras encontradas: " << cont4;
    qDebug() << "Palabras de 3 letras encontradas: " << cont3;
    qDebug() << "Palabras de 2 letras encontradas: " << cont2;

    return salida;
}

//Esta funcion busca la primera palabra mas larga que encuentra y la devuelve
//Es mucho mas rapido que el metodo anterior pero solo devuelve una palabra
QString Diccionario::buscarPalabraMasLarga(QString texto)
{
    //Ordenamos las letras
    const QString letras(ordenaLetrasPalabra(texto));

    QStringList listaTempReales;
    QStringList listaTempOrdenadas;
    QStringList palabrasRevisadas;

    //-- Comprobamos si hay alguna palabra de 9 letras -----------------------------------

    listaTempReales = palabrasDe9_.first;
    listaTempOrdenadas = palabrasDe9_.second;

    //qDebug() << " BUSCAMOS AHORA LAS PALABRAS DE 9 CON LAS LETRAS: " <<letras;
    if (listaTempOrdenadas.contains(letras))
    {
        int index = listaTempOrdenadas.indexOf(letras);
        return listaTempReales.at(index);
    }

    //-- Comprobamos si hay alguna de 8 -----------------------------------------------

    listaTempReales = palabrasDe8_.first;
    listaTempOrdenadas = palabrasDe8_.second;
    for (int i=0;i<letras.count();i++)
    {
        QString auxLetras;
        auxLetras = letras;
        auxLetras.remove(i,1);
        if (!palabrasRevisadas.contains(auxLetras))
        {
            //qDebug() << " BUSCAMOS AHORA LAS PALABRAS DE 8 CON LAS LETRAS: " <<auxLetras;
            if (listaTempOrdenadas.contains(auxLetras))
            {
                int index = listaTempOrdenadas.indexOf(auxLetras);
                return listaTempReales.at(index);
            }
            palabrasRevisadas << auxLetras;
        }
    }
    //-- Comprobamos si hay alguna de 7 -----------------------------------------------

    listaTempReales = palabrasDe7_.first;
    listaTempOrdenadas = palabrasDe7_.second;
    palabrasRevisadas.clear();
    for (int i=0;i<letras.count();i++)
    {
        QString auxLetras;
        auxLetras = letras;
        auxLetras.remove(i,1);
        for (int j=0;j<auxLetras.count();j++)
        {
            QString auxLetras2;
            auxLetras2 = auxLetras;
            auxLetras2.remove(j,1);
            if (!palabrasRevisadas.contains(auxLetras2))
            {
              //qDebug() << " BUSCAMOS AHORA LAS PALABRAS DE 7 CON LAS LETRAS: " <<auxLetras2;
                while (listaTempOrdenadas.contains(auxLetras2))
                {
                    int index = listaTempOrdenadas.indexOf(auxLetras2);
                    return listaTempReales.at(index);
                }
                palabrasRevisadas << auxLetras2;
            }
        }
    }

    //-- Comprobamos si hay alguna de 6 -----------------------------------------------

    listaTempReales = palabrasDe6_.first;
    listaTempOrdenadas = palabrasDe6_.second;
    palabrasRevisadas.clear();
    for (int m=0;m<letras.count();m++)
    {
        QString auxLetras;
        auxLetras = letras;
        auxLetras.remove(m,1);
        for (int i=0;i<auxLetras.count();i++)
        {
            QString auxLetras2;
            auxLetras2 = auxLetras;
            auxLetras2.remove(i,1);
            for (int j=0;j<auxLetras2.count();j++)
            {
                QString auxLetras3;
                auxLetras3 = auxLetras2;
                auxLetras3.remove(j,1);
                if (!palabrasRevisadas.contains(auxLetras3))
                {
                    //qDebug() << " BUSCAMOS AHORA LAS PALABRAS DE 6 CON LAS LETRAS: " <<auxLetras3;
                    if (listaTempOrdenadas.contains(auxLetras3))
                    {
                        int index = listaTempOrdenadas.indexOf(auxLetras3);
                        return listaTempReales.at(index);
                    }
                    palabrasRevisadas << auxLetras3;
                }
            }
        }
    }

    //-- Comprobamos si hay alguna de 5 -----------------------------------------------

    listaTempReales = palabrasDe5_.first;
    listaTempOrdenadas = palabrasDe5_.second;
    palabrasRevisadas.clear();
    for (int l=0;l<letras.count();l++)
    {
        QString auxLetras;
        auxLetras = letras;
        auxLetras.remove(l,1);
        for (int m=0;m<auxLetras.count();m++)
        {
            QString auxLetras2;
            auxLetras2 = auxLetras;
            auxLetras2.remove(m,1);
            for (int i=0;i<auxLetras2.count();i++)
            {
                QString auxLetras3;
                auxLetras3 = auxLetras2;
                auxLetras3.remove(i,1);
                for (int j=0;j<auxLetras3.count();j++)
                {
                    QString auxLetras4;
                    auxLetras4 = auxLetras3;
                    auxLetras4.remove(j,1);
                    if (!palabrasRevisadas.contains(auxLetras4))
                    {
                        //qDebug() << " BUSCAMOS AHORA LAS PALABRAS DE 5 CON LAS LETRAS: " <<auxLetras4;
                        if (listaTempOrdenadas.contains(auxLetras4))
                        {
                            int index = listaTempOrdenadas.indexOf(auxLetras4);
                            return listaTempReales.at(index);
                        }
                        palabrasRevisadas << auxLetras4;
                    }
                }
            }
        }
    }  

    //-- Comprobamos si hay alguna de 4 -----------------------------------------------

    listaTempReales = palabrasDe4_.first;
    listaTempOrdenadas = palabrasDe4_.second;
    palabrasRevisadas.clear();
    for (int l=0;l<letras.count();l++)
    {
        QString auxLetras;
        auxLetras = letras;
        auxLetras.remove(l,1);
        for (int m=0;m<auxLetras.count();m++)
        {
            QString auxLetras2;
            auxLetras2 = auxLetras;
            auxLetras2.remove(m,1);
            for (int i=0;i<auxLetras2.count();i++)
            {
                QString auxLetras3;
                auxLetras3 = auxLetras2;
                auxLetras3.remove(i,1);
                for (int j=0;j<auxLetras3.count();j++)
                {
                    QString auxLetras4;
                    auxLetras4 = auxLetras3;
                    auxLetras4.remove(j,1);
                    for (int k=0;k<auxLetras4.count();k++)
                    {
                        QString auxLetras5;
                        auxLetras5 = auxLetras4;
                        auxLetras5.remove(k,1);
                        if (!palabrasRevisadas.contains(auxLetras5))
                        {
                            //qDebug() << " BUSCAMOS AHORA LAS PALABRAS DE 4 CON LAS LETRAS: " <<auxLetras5;
                            if (listaTempOrdenadas.contains(auxLetras5))
                            {
                                int index = listaTempOrdenadas.indexOf(auxLetras5);
                                return listaTempReales.at(index);
                            }
                            palabrasRevisadas << auxLetras5;
                        }
                    }
                }
            }
        }
    }

    //-- Comprobamos si hay alguna de 3 -----------------------------------------------

    listaTempReales = palabrasDe3_.first;
    listaTempOrdenadas = palabrasDe3_.second;
    palabrasRevisadas.clear();
    for (int l=0;l<letras.count();l++)
    {
        QString auxLetras;
        auxLetras = letras;
        auxLetras.remove(l,1);
        for (int m=0;m<auxLetras.count();m++)
        {
            QString auxLetras2;
            auxLetras2 = auxLetras;
            auxLetras2.remove(m,1);
            for (int i=0;i<auxLetras2.count();i++)
            {
                QString auxLetras3;
                auxLetras3 = auxLetras2;
                auxLetras3.remove(i,1);
                for (int j=0;j<auxLetras3.count();j++)
                {
                    QString auxLetras4;
                    auxLetras4 = auxLetras3;
                    auxLetras4.remove(j,1);
                    for (int k=0;k<auxLetras4.count();k++)
                    {
                        QString auxLetras5;
                        auxLetras5 = auxLetras4;
                        auxLetras5.remove(k,1);
                        for (int a=0;a<auxLetras5.count();a++)
                        {
                            QString auxLetras6;
                            auxLetras6 = auxLetras5;
                            auxLetras6.remove(a,1);
                            if (!palabrasRevisadas.contains(auxLetras6))
                            {
                                //qDebug() << " BUSCAMOS AHORA LAS PALABRAS DE 3 CON LAS LETRAS: " <<auxLetras6;
                                if (listaTempOrdenadas.contains(auxLetras6))
                                {
                                    int index = listaTempOrdenadas.indexOf(auxLetras6);
                                    return listaTempReales.at(index);
                                }
                                palabrasRevisadas << auxLetras6;
                            }
                        }
                    }
                }
            }
        }
    }

    //-- Comprobamos si hay alguna de 2 -----------------------------------------------

    listaTempReales = palabrasDe2_.first;
    listaTempOrdenadas = palabrasDe2_.second;
    palabrasRevisadas.clear();
    for (int l=0;l<letras.count();l++)
    {
        QString auxLetras;
        auxLetras = letras;
        auxLetras.remove(l,1);
        for (int m=0;m<auxLetras.count();m++)
        {
            QString auxLetras2;
            auxLetras2 = auxLetras;
            auxLetras2.remove(m,1);
            for (int i=0;i<auxLetras2.count();i++)
            {
                QString auxLetras3;
                auxLetras3 = auxLetras2;
                auxLetras3.remove(i,1);
                for (int j=0;j<auxLetras3.count();j++)
                {
                    QString auxLetras4;
                    auxLetras4 = auxLetras3;
                    auxLetras4.remove(j,1);
                    for (int k=0;k<auxLetras4.count();k++)
                    {
                        QString auxLetras5;
                        auxLetras5 = auxLetras4;
                        auxLetras5.remove(k,1);
                        for (int a=0;a<letras.count();a++)
                        {
                            QString auxLetras6;
                            auxLetras6 = auxLetras5;
                            auxLetras6.remove(a,1);
                            for (int b=0;b<auxLetras6.count();b++)
                            {
                                QString auxLetras7;
                                auxLetras7 = auxLetras6;
                                auxLetras7.remove(b,1);
                                if (!palabrasRevisadas.contains(auxLetras7))
                                {
                                    //qDebug() << " BUSCAMOS AHORA LAS PALABRAS DE 2 CON LAS LETRAS: " <<auxLetras7;
                                    if (listaTempOrdenadas.contains(auxLetras7))
                                    {
                                        int index = listaTempOrdenadas.indexOf(auxLetras7);
                                        return listaTempReales.at(index);
                                    }
                                    palabrasRevisadas << auxLetras7;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    return "";
}
