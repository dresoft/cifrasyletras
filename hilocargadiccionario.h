#ifndef HILOCARGADICCIONARIO_H
#define HILOCARGADICCIONARIO_H

#include <QThread>

class HiloCargaDiccionario : public QThread
{
public:
    HiloCargaDiccionario();

    void run();
signals:

    void hiloCargado();
public slots:
};

#endif // HILOCARGADICCIONARIO_H
