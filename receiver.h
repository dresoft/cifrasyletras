#ifndef RECEIVER_H
#define RECEIVER_H

#include <QObject>
#include <QString>

class Receiver : public QObject
{
  Q_OBJECT
  Q_PROPERTY(QString valorCerebro READ valorCerebro WRITE setValorCerebro NOTIFY valorCerebroChanged)
  Q_PROPERTY(QStringList listaOperaciones READ listaOperaciones)
  Q_PROPERTY(QStringList listaNumeros READ listaNumeros)
  Q_PROPERTY(QString nombreOro READ nombreOro WRITE setNombreOro )
  Q_PROPERTY(QString puntosOro READ puntosOro WRITE setpuntosOro NOTIFY puntosOroChanged)
  Q_PROPERTY(QString nombrePlata READ nombrePlata WRITE setNombrePlata )
  Q_PROPERTY(QString puntosPlata READ puntosPlata WRITE setpuntosPlata NOTIFY puntosPlataChanged)
  Q_PROPERTY(QString nombreBronce READ nombreBronce WRITE setNombreBronce )
  Q_PROPERTY(QString puntosBronce READ puntosBronce WRITE setpuntosBronce NOTIFY puntosBronceChanged)
  public:

    explicit Receiver(QObject *parent = 0);
    QString getValorLogradoCerebro();
    void setValorCerebro(QString valor);
    QString valorCerebro();
    QStringList listaOperaciones();
    QStringList listaNumeros();
    QString nombreOro();
    QString puntosOro();
    QString nombrePlata();
    QString puntosPlata();
    QString nombreBronce();
    QString puntosBronce();

    QStringList pintarNumeros(QStringList lista);

    QString cargarRecords();
    bool checkDirs();
    QString getDataPath();
private:

    QList<int> numeros_;
    int exacto_;
    QString valorLogradoCerebro_;
    QString letras_;
    QStringList listaOperaciones_;
    QStringList listaNumeros_;
    QString nombreOro_;
    QString puntosOro_;
    QString nombrePlata_;
    QString puntosPlata_;
    QString nombreBronce_;
    QString puntosBronce_;

  signals:

    void sendToQml(int count);
    void valorCerebroChanged();
    void puntosOroChanged();
    void puntosPlataChanged();
    void puntosBronceChanged();
  //los metodos llamados desde qml deben ser public slots
  public slots:

    bool existePalabra(QString palabra);
    QString buscaPalabra();
    int buscarExacto();
    void setExacto(int valor);
    void addCifra(int valor);
    void addLetra(QString valor);
    void resetCifras();
    void resetLetras();
    void setNombreOro(QString valor);
    void setpuntosOro(QString valor);
    void setNombrePlata(QString valor);
    void setpuntosPlata(QString valor);
    void setNombreBronce(QString valor);
    void setpuntosBronce(QString valor);
    bool guardarRecords();
    void salirDelJuego();
    bool reiniciarRecords();
    bool setRecords();
    bool abrirNavegador(QString url);
    bool enviarMail();
    QString eliminarCaracteresPeligrosos(QString entrada);
};
#endif // RECEIVER_H
