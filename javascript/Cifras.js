.pragma library //unica instancia compartida por todos los archivos QML que la usen

//CIFRAS INICIALES Y RESTANTES
var cifrasIniciales = [1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,25,25,50,50,75,75,100,100]

var cifrasRestantes = []

//Valores de la partida a jugar
var cifrasPartida = []
var unidades = 0
var decenas = 0
var centenas = 0
var exacto = 0
//

//JUGADAS ANTERIORES DE LA PARTIDA (Cada jugada es un array de 6 elementos que almacena el valor
//          de las Cifras del gridCifras. Si vale 0 es que no hay cifra
//
var jugadaActual = 0
var jugadasAnteriores = []


//-- FUNCIONES de MANEJO de las CIFRAS -------------------------------------------------------

function getCifra()
{
    //Si es la primera cifra pedida inicializamos los arrays
    if(cifrasRestantes.length == 0)
    {
        reset();
    }

    var i = randomDe(cifrasRestantes.length);
    var salida = cifrasRestantes[i]

    //eliminamos el elemento para que no pueda volver a ser escogido
    cifrasRestantes.splice(i,1);

    //console.log("getCifra() "+salida)
    return salida;
}

function getCifraPartida()
{
    console.log("getCifraPartida length:"+cifrasPartida.length)
    console.log("cifras concretas = "+cifrasPartida)
    if(cifrasPartida.length > 0)
        return cifrasPartida.pop()
}

//-- FUNCIONES de MANEJO de las JUGADAS -------------------------------------------------------

function pushJugada(jugada) {

    console.log("guardarJugada("+jugada+")");
    if (jugadaActual > 4)
        return;

    jugadasAnteriores.push(jugada)
    jugadaActual++;
//    console.log("JUGADA ACTUAL(I) :" + jugadaActual)
}

function popJugada()
{
    console.log("popJugada("+jugadaActual+")");
    if (jugadaActual < 0)
        return;

    jugadaActual--;
//    console.log("JUGADA ACTUAL(II) :" + jugadaActual)
    return jugadasAnteriores.pop()
}

//Elimina todas las jugadas y devuelve la jugada inicial
function popJugadaInicial()
{
    console.log("popJugadaInicial("+jugadaActual+")")
    if (jugadaActual < 1)
        return -1;

    //Si es la jugada 1 no entra en el while
    while (jugadaActual>1)
    {
        jugadaActual--;
        jugadasAnteriores.pop()
    }

    //devolvemos la jugada inicial
    jugadaActual--;
    console.log("JUGADA ACTUAL(III) :" + jugadaActual)
    return jugadasAnteriores.pop()
}

// -- FUNCIONES de CONTROL de PARTIDA -----------------------------------
function reset()
{
    cifrasRestantes = []
    cifrasPartida = []
    for (var i=0;i<cifrasIniciales.length;i++)
    {
        cifrasRestantes.push(cifrasIniciales[i])
    }

    //console.log("CIFRAS RESTANTES DESPUES: " + cifrasRestantes)
    jugadaActual = 0
    unidades = 0
    decenas = 0
    centenas = 0
    exacto = 0
}

// -- FUNCIONES de CALCULO MATEMATICO --------------------------------------------

//Funcion que devuelve verdadero si un numero es divisor de otro
function esDivisor (valor1, valor2)
{
    var valorMayor;
    var valorMenor;
    var salida = false;

    if (valor1 > valor2)
    {
        valorMayor = valor1;
        valorMenor = valor2;
    }
    else
    {
        valorMayor = valor2;
        valorMenor = valor1;
    }

    if ((valorMayor % valorMenor) === 0)
    {
        salida = true
    }

    return salida;
}

//funcion que realiza operaciones matematicas
function calcular (valor1, valor2, operacion)
{
    var resultado = -1;

    switch (operacion)
    {
        //no definido
        case 0:
             break;
        //suma
        case 1:
             resultado = valor1 + valor2;
             break;
        //resta
       case 2:
            if (valor2 > valor1)
                resultado = valor2 - valor1;
            else
               resultado = valor1 - valor2;
            break;
        //multiplicacion
        case 3:
            resultado = valor1 * valor2;
            break;
        //division
        case 4:
            if (esDivisor(valor1, valor2))
            {
                if (valor2 > valor1)
                    resultado = valor2 / valor1;
                else
                    resultado = valor1 / valor2;
            }
            else
            {
                console.log ("No son divisor uno del otro");
            }
           break;
        case 5:
        default:
            break;
    }
 return resultado;
}

function randomDe(cantidad)
{
    return Math.floor(Math.random() * cantidad);
}

function setExacto (exacto)
{
    var aux = exacto;
    if (aux < 100)
        aux = aux + 100
    unidades = exacto % 10
    aux = (aux - unidades)/10
    decenas = aux % 10
    aux = (aux - decenas)/10
    centenas = aux
    console.log ("Cifras.setExacto("+exacto+") - centenas="+centenas+"; decenas= "+decenas+"; unidades="+unidades)
}

//Genera una partida y devuelve una cadena con los datos "exacto;num1;num2;num3;num4;num5;num6"
function generaPartida ()
{
    reset()
    //generamos los 6 numeros con los que buscar el exacto
    for (var i=0;i<6;i++)
        cifrasPartida.push(getCifra())
    //generamos el exacto
    unidades = randomDe(10)
    decenas = randomDe(10)
    centenas = randomDe(9) + 1

    exacto = centenas*100+decenas*10+unidades
    console.log("== PARTIDA GENERADA ==")
    console.log("EXACTO = "+exacto)
    console.log("Numeros = "+ cifrasPartida)

    var datosPartida = generaDatosPartida()
    console.log("Datos partida = "+datosPartida)
    return datosPartida
}

//generamos los datos de la partida para poder compartirlo como una cadena
//3 cifras exacto + 2 cifras por cada uno de los 6 numeros con los que buscar el exacto = 15 cifras
function generaDatosPartida()
{
    var salida = exacto.toString()
    for (var i=0;i<cifrasPartida.length;i++)
        salida = salida+aDosDigitos(cifrasPartida[i])

    return salida
}

//Esta funcion inserta los ceros necesarios para que tengan 3 digitos los numeros
function aTresDigitos(numero)
{
    var cadena = ""
    if (numero === 0)
        cadena = "000"
    else if (numero < 10)
        cadena = "00"+numero.toString()
    else if (numero < 100)
        cadena = "0"+numero.toString()
    else
        cadena = numero.toString()

    return cadena
}

//Esta funcion inserta los ceros necesarios para que tengan 2 digitos los numeros,
//en caso de que el numero sea 100, se pondran solo dos ceros
function aDosDigitos(numero)
{
    var cadena = ""
    if (numero < 10)
        cadena = "0"+numero.toString()
    else if (numero < 100)
        cadena = numero.toString()
    else //100
        cadena = "00"

    return cadena
}

//
function setPartida(datosPartida)
{
    reset()
    setExacto(parseInt(datosPartida.substring(0,3)))
    for (var i=0;i<6;i++)
    {
        var valor = ""
        valor = datosPartida.substring((2*i)+3,(2*i)+5)
        if (valor === "00")
            cifrasPartida.push(100)
        else
            cifrasPartida.push(parseInt(valor))
    }
}

//Comprimimos la cifra en base 36
function comprimeCifra(datosPartida)
{
    var number = parseInt(datosPartida)
    console.log("numero inicial= "+number)
    var numHex = number.toString(36)
    console.log("Base 36   = "+numHex)
    return numHex
}

//Descomprimimos la cifra en base 36
function descomprimeCifra(valor)
{
    console.log("numero inicial= "+parseInt(valor,36))
    return parseInt(valor,36)
}
