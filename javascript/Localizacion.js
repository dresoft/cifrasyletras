.pragma library //unica instancia compartida por todos los archivos QML que la usen

//ARCHIVO DE LOCALIZACION =========================================================

//Idiomas, ingles y español(por defecto)
var IdiomasDisponibles = {
    ESP: [0,1],
    ENG: 1
}

var idiomaActual = IdiomasDisponibles.ESP

//Cadenas de textos
var cadenas = {
    CYLETRAS: ["Cifras & Letras","Numbers & Letters"],
    DESHACER: ["Deshacer","Undo"],
    COMENZAR: ["Comenzar","Begin"]
}

// Funciones de manejo

