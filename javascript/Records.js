.pragma library

var puntuacionOro = 0
var puntuacionPlata = 0
var puntuacionBronce = 0
var puestoLogrado = 0
var nombreOro = "---"
var nombrePlata = "---"
var nombreBronce = "---"

function cargarPuntuacionOro(oro,nombre)
{
    puntuacionOro = oro
    nombreOro = nombre
}

function cargarPuntuacionPlata(plata,nombre)
{
    puntuacionPlata = plata
    nombrePlata = nombre
}

function cargarPuntuacionBronce(bronce,nombre)
{
    puntuacionBronce = bronce
    nombreBronce = nombre
}

//Comprobamos el puesto logrado
function setPuestoLogrado(resultado)
{
    var salida = 4
    if (resultado > puntuacionOro)
        salida = 1
    else if (resultado > puntuacionPlata)
        salida = 2
    else if (resultado > puntuacionBronce)
        salida = 3

    puestoLogrado = salida
}

//Devuelve 1 si nuevo oro
//Devuelve 2 si nueva plata
//Devuelve 3 si nuevo bronce
//Devuelve 4 si no entra el ranking
//Ordena el resto de records
function guardarRecord(resultado,nombre)
{
    var salida = 4
    if (resultado > puntuacionOro)
    {
        puntuacionBronce = puntuacionPlata
        nombreBronce = nombrePlata
        puntuacionPlata = puntuacionOro
        nombrePlata = nombreOro
        puntuacionOro = resultado
        nombreOro = nombre
        salida = 1
    }
    else if (resultado > puntuacionPlata)
    {
        puntuacionBronce = puntuacionPlata
        nombreBronce = nombrePlata
        puntuacionPlata = resultado
        nombrePlata = nombre
        salida = 2
    }
    else if (resultado > puntuacionBronce)
    {
        puntuacionBronce = resultado
        nombreBronce = nombre
        salida = 3
    }
    puestoLogrado = salida
    return salida
}
