TEMPLATE = app

QT += qml quick widgets
#QT += androidextras

SOURCES += main.cpp \
    algoritmos.cpp \
    receiver.cpp \
    diccionario.cpp \
    hilocargadiccionario.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android

HEADERS += \
    algoritmos.h \
    receiver.h \
    diccionario.h \
    hilocargadiccionario.h

OBJECTIVE_SOURCES +=

