import QtQuick 2.3
import QtQuick.Controls 1.2
//import QtGraphicalEffects 1.0

Item {

    id: root
    signal cambiarQml (string url)  //SEÑAL COMPARTIDA por todas las pantallas

    Rectangle {
        id: fondo
        anchors.fill: parent
        color: "#333333"
    }

// -- TITULO ----- 18% ---------------------------------------------------------------
//    Text {
//        id: titulo
//        anchors.left: root.left
//        anchors.leftMargin: root.width*0.04
//        anchors.top: root.top
//        height: root.height*0.18
//        width: root.width*0.4
//        verticalAlignment: Text.AlignVCenter
//        horizontalAlignment: Text.AlignHCenter
//        text: " Números"
//        font.family: "Ubuntu"
//        font.bold: true
//        font.pixelSize: root.width *0.09
//        color: "silver"
//    }

//    Text {
//        id: titulo2
//        anchors.left: root.left
//        anchors.top: root.top
//        anchors.topMargin: root.height*0.02
//        anchors.horizontalCenter: parent.horizontalCenter
//        height: root.height*0.18
//        width: root.width*0.2
//        verticalAlignment: Text.AlignVCenter
//        horizontalAlignment: Text.AlignHCenter
//        text: "&"
//        font.family: "Ubuntu"
//        font.bold: true
//        font.pixelSize: root.width *0.2
//        color: "red"
//    }

//    Text {
//        id: titulo3
//        anchors.top: root.top
//        anchors.right: root.right
//        anchors.rightMargin: root.width*0.07
//        height: root.height*0.16
//        width: root.width*0.4
//        horizontalAlignment: Text.AlignHCenter
//        verticalAlignment: Text.AlignBottom
//        text: "Letras"
//        font.family: "Ubuntu"
//        font.bold: true
//        font.pixelSize: root.width *0.1
//        color: "gold"
//    }


    Item {
        id: titulo
        anchors.top: root.top
        anchors.horizontalCenter: parent.horizontalCenter
        height: root.height * 0.18
        width: root.width

    }

    Image {
        id: logo
        source: "qrc:/images/logoapp.png"
        anchors.top: root.top
        anchors.topMargin: root.height*0.03
        anchors.horizontalCenter: root.horizontalCenter
        anchors.bottomMargin: root.height*0.03
        height: root.height*0.20
        width: root.width
        fillMode: Image.PreserveAspectFit
    }

// -- VISTA CENTRAL 82% ------------------------------------------------------------------

    GridView {

        id: vistaCentral
        anchors.top: logo.bottom
        anchors.topMargin: root.height*0.05
        anchors.horizontalCenter: root.horizontalCenter
        width: root.width*0.95
        height: root.height*0.78
        model: ModeloMenuPrincipal{}
        cellWidth: width/2
        cellHeight: height/ 3
        interactive: false // no flickable

        property bool activo: false

        delegate: DelegateMenuPrincipal {

                    height: vistaCentral.cellHeight
                    width: vistaCentral.cellWidth
                    urlQml: qml
                    urlImagen: fuenteImagen
                    textoBoton: texto

                    onUpdateLoader: root.cambiarQml(url)
                }
    }

    // Botonera 20%

    Item {
        id: botonera
        height: root.height*0.2
        width: root.width*0.9
        anchors.horizontalCenter: root.horizontalCenter
        anchors.bottom: root.bottom


        BotonImagen {
            id: salir
            height: parent.height*0.6
            width: height
            anchors.right: parent.right
            anchors.verticalCenter: parent.verticalCenter
            imagePath: "qrc:/images/images/salir.png"
            onPulsado: receiver.salirDelJuego()
        }

        BotonImagen {
            id: info
            height: parent.height*0.4
            width: height
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            imagePath: "qrc:/images/images/info.png"
            onPulsado: {
                if (dialogoInfo.visible === true)
                    dialogoInfo.visible = false
                else
                    dialogoInfo.visible = true
            }
        }

        Info {
            id: dialogoInfo
            visible: false
            anchors.left: info.right
            anchors.leftMargin: height*0.15
            anchors.right: salir.left
            anchors.rightMargin: height*0.15
            height: parent.height*0.5
            anchors.verticalCenter: info.verticalCenter
            onVisibleChanged: {
                if (visible)
                    info.imagePath = "qrc:/images/images/cerrar.png"
                else
                    info.imagePath = "qrc:/images/images/info.png"

            }
        }
    }
}
