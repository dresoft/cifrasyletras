import QtQuick 2.3
import QtQuick.Controls 1.2

Item {

    id:root
    width: parent.width*0.4
    height: parent.height*0.6

    state: "oculto"
    signal desplegado(int pixelesAncho);
    signal escondido;

    //pequeño rectangulo que incluye la imagen de una flecha y un mousearea
    Rectangle {
        id: flecha
        anchors.left: base.right
        //topMargin negativo para no mostrar la parte superior del rectagulo y que no se vean las esquinas redondeadas
        anchors.leftMargin: -root.width*0.05
        anchors.verticalCenter: root.verticalCenter
        height: root.height*0.25
        width: root.width*0.2
        color: "white"
        opacity: 0.7
        radius: 4
        border.width: 1
        border.color: "gray"

        Image {

            id: imagen
            anchors.right: flecha.right
            anchors.verticalCenter: flecha.verticalCenter
            width: flecha.width -root.width/20
            height: width

            source: "qrc:/images/images/flechas.png"
         }

        MouseArea {
            id: mouseAreaFlecha
            anchors.fill: parent
            onClicked: {
                if (root.state == "oculto")
                {
                    root.state = "visible";
                    desplegado(base.width);
                }
                else
                {
                    root.state = "oculto";
                    escondido();
                }
            }
        }
    }

    //Rectangulo mas grande donde situar los botones o listviews
    Rectangle
    {
        id: base
        width: 0
        height:parent.height
        color: "white"
        opacity: 0.8


        MouseArea {
             id: mouseAreaBase
             anchors.fill: parent
        }
    }

    states: [
        State {name: "oculto"},
        State {name: "visible"}
    ]


    //Al cambiar de estado modificamos el ancho del rectangulo grande y giramos la direccion de la imagen de las flechas
    transitions: [
        Transition {
            from: "oculto"
            to: "visible"
            NumberAnimation { target: base; properties: "width"; to: root.width - flecha.width; duration: 250 }
            RotationAnimation { target: imagen; duration: 250; from: 0 ; to: 180; direction: RotationAnimation.Clockwise }
        },
        Transition {
            from: "visible"
            to: "oculto"
            NumberAnimation { target: base; properties: "width"; to: 0; duration: 250 }
            RotationAnimation { target: imagen; duration: 250; from: 180 ; to: 0; direction: RotationAnimation.Counterclockwise }
        }
    ]

    Component.onCompleted: { }
}
