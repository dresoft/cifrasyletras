// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 2.3
import QtQuick.Controls 1.2

import "../javascript/Cifras.js" as Cifras
import "../javascript/ConfiguracionGeneral.js" as Conf

Item {

    id: root
    property int numeroDeFicha: 0  //numero ordinal de cifra q se esta sacando, comienza en 0

    //seleccionadas ------------
    property int indiceCifraSeleccionada: -1 //indice de la cifra seleccionada
    property bool cifraSeleccionada: false
    property int indiceOperacionSeleccionada : -1
    property bool operacionSeleccionada: false
    // --------------------------

    property int valorInicialCuentaAtras: Conf.tiempoEntrenamiento  //tiempo en segundos de la cuenta atras
    property int valorMasCercano: 0 //valor mas cercano al exacto que sera mostrado en masCercano
    property int exacto //numero que hay que intentar conseguir

    property bool partidaPausada: false
    property bool partidaTerminada: false
    property bool empezarEnPausa: false

    signal cambiarQml (string url)  //SEÑAL COMPARTIDA por todas las pantallas
    signal ayuda (int idPantalla)   //SEÑAL COMPARTIDA por todas las pantallas

    property int tamañoFuenteMediana: height*0.023
    property int tamañoFuenteTitulo: height*0.023

    Rectangle {
        id: fondo
        anchors.fill: parent
        color: "#333333"
    }

 //-- ZONA SUPERIOR DE LA PANTALLA (SacaCifras) ----- 20% (1/5) Alto --------------
  
    SacaCifras {
        id: sacacifras

        anchors.horizontalCenter: root.horizontalCenter
        width: root.width * 0.9
        height: root.height*0.17
        anchors.top: root.top
        anchors.topMargin: root.height*0.03
        todasSacadas: false
        onCifraSacada: {

            if (todasSacadas)
            {
                //console.log("Todas las cifras sacadas")
            }
            else
            {
                //actualizamos el modelo con las cifras sacadas
                agregarCifra(value)
                numeroDeFicha++
            }
        }

        onScfinalizado: {
            crono.activo = true
            crono.arrancarPararCrono()
            gridCifras.activo = true
            exacto = valExacto
            console.log("-- EXACTO: " + valExacto + " -- ")

            //ENVIAMOS EL EXACTO A RECEIVER PARA QUE COMIENCE A CALCULAR
            receiver.setExacto(exacto)
            console.log("===== INICIO PARTIDA =====")
            if(empezarEnPausa)
                pausarPartida()
        }
    }

    CuentaAtras
    {
        id: crono

        activo: false
        //Cuadrado
        height: root.height/9
        width: height
        anchors.top: root.top
        anchors.right: root.right
        anchors.rightMargin: root.width/20
        anchors.topMargin: root.height/30
        valorInicialSegundos: valorInicialCuentaAtras

        onRunningChanged:
            if (running)
                sacacifras.activo = false

        onTiempoAgotado: {
            setValorLogradoCerebro()
            gridCifras.activo = false //desactivamos la fila de cartas porque se ha acabado el tiempo
            deseleccionarTodo()
            sobrefondo.visible = true
            exactoNoEncontrado.visible = true
            botonera.activo = false
            acabarPartida()
        }

        onQuedan10segs: masTiempo.visible = true
    }

    Rectangle {

        id: masTiempo
        anchors.top: crono.bottom
        anchors.topMargin: height*0.45
        anchors.horizontalCenter: crono.horizontalCenter
        height: crono.height*0.4
        color: "green"
        width: crono.width*1.2
        radius: 20
        border.color: "white"
        border.width: 1
        visible: false //Solo sera visible cuando falten 10 segundos

        Text {

            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            text: "+ TIEMPO"
            font.pixelSize: height*0.5
            font.bold: true
            color: "white"
        }

        MouseArea {
            anchors.fill: parent
            onClicked: if (crono.activo) {crono.restart(); masTiempo.visible = false}
        }
    }

//-- ZONA MEDIA DE LA PANTALLA -- 50% Alto -- (GridCifras 33% Calculadora 20%) ---
    Rectangle {
        id:fondoGrid
        anchors.fill: gridCifras
        radius: 4
        color: "gray"
        visible: gridCifras.visible ? true : false
    }

    GridView {

        id: gridCifras
        model: modeloCifras
        width:  root.width*0.9
        height: root.height*0.3
        anchors.top: sacacifras.bottom
        anchors.topMargin: root.height*0.03
        anchors.horizontalCenter: parent.horizontalCenter
        cellWidth: width/3
        cellHeight: height/2

        interactive: false //no flickable

        property bool activo: false

        delegate: Cifra {
                        height: gridCifras.cellHeight
                        width: gridCifras.cellWidth
                        text: valor
                        visible: (valor == 0) ? false : true
                        indice: index
                        activo: gridCifras.activo //se activara cuando la GridView este activa
                        onCifraPulsada: {cambiaSeleccion(index)}
                  }
    }

    Rectangle {
        id: pausaSobreGrid
        anchors.fill: gridCifras
        visible: partidaPausada
        color: "orangered"
        radius: 4

        Text {
            anchors.centerIn: parent
            font.pixelSize: parent.height/3
            color: "white"
            text: "EN PAUSA"
            font.family: "Ubuntu Mono"
        }
    }

    //Botones de calculo
    Calculadora
    {
        id: calculadora
        activo : false
        width: sacacifras.width
        height: root.height*0.14
        anchors.horizontalCenter: root.horizontalCenter
        anchors.top: gridCifras.bottom
        anchors.topMargin: root.height*0.03

        onCambioOperacionSeleccionada: {
               operacionSeleccionada = selected
            if (selected)
                indiceOperacionSeleccionada = numOper
            else
                indiceOperacionSeleccionada = -1
        }
    }

//-- PARTE INFERIOR DE LA PANTALLA (Botonera) ----- 25% Alto  (Sobra el 5%) -------

    BotoneraCifras {
        id: botonera

        activo: false
        width: root.width*0.9
        height: root.height*0.2
        anchors.bottom: root.bottom
        anchors.bottomMargin: root.height*0.05
        anchors.horizontalCenter: root.horizontalCenter
        labelMasCercano: valorMasCercano

        onPulsada: {
            if (op == "UNDO")
            {
                console.log("-- UNDO --")
                deshacerJugada()
                if (Cifras.jugadaActual == 0)
                    botonera.activo = false
            }
            else if (op == "RESTART")
            {
                console.log("-- RESTART --")
                cargarJugadaInicial()
                botonera.activo = false
            }
        }
    }

    BotonImagen {
        id: botonPlay
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: botonera.top
        visible: false
        height: parent.height*0.14
        width: height
        imagePath: "qrc:/images/images/play.png"
        onPulsado: {
            pausarPartida()
        }
    }

    //Comprueba si una cifra es la mas cercana al valor exacto
    function compruebaValorMasCercano(valor)
    {
        if (Math.abs(exacto - valor) < Math.abs(exacto - valorMasCercano))
        {
            valorMasCercano = valor
        }
    }

// -- MENSAJES sobre la PANTALLA ------------------------------------------------

    //SOBREFONDO para oscurecer la pantalla
    Rectangle {
        id: sobrefondo
        anchors.fill: root
        visible: false
        color: "black"
        opacity: 0.7

        MouseArea {
            anchors.fill: parent
        }
    }

    Intro
    {
        id: intro
        anchors.fill: root

        onTiempoAgotado: {
            visible = false
            sacacifras.ejecutar()
        }
    }


    //EXACTO ENCONTRADO
    Rectangle {

        id: exactoEncontrado
        anchors.centerIn: root
        visible: false
        width: root.width/1.5
        height: root.height/2
        radius: 4
        color: "green"
        border.width: 2
        border.color: "white"

        property string numeroCerebro: textoCerebroResultadoExacto.text

        Item {

            id: copaImagen
            height: parent.height*0.2
            width: height
            anchors.top: exactoEncontrado.top
            anchors.left: exactoEncontrado.left

            Image {
                id: img
                source: "qrc:/images/images/copa.png"
                anchors.centerIn: parent
                height: parent.width*0.8
                width: height
            }

            Text {
                id: textoTiempoAgotadoExacto
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.right
                height: parent.height/2
                text: "¡EXACTO ENCONTRADO!"
                color: "white"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: tamañoFuenteTitulo
                font.bold: true

            }
        }

        Text {
            id: textoExactoEncontrado
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width*0.9
            height: parent.height*0.25
            anchors.top: copaImagen.bottom
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.Wrap
            text: "Has conseguido el número:"
            color: "white"
            font.pixelSize: tamañoFuenteMediana
            font.bold: true

            Rectangle {
                width: parent.width*0.6
                height: parent.height/3
                color: "black"
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter

                Text {
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    text: valorMasCercano
                    font.pixelSize: copaImagen.height*0.3
                    font.bold: true
                }
            }
        }

        Text {
            id: centroExacto
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width*0.9
            height: parent.height*0.25
            anchors.top: textoExactoEncontrado.bottom
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.Wrap
            text: "Cerebro ha conseguido:"
            color: "white"
            font.pixelSize: tamañoFuenteMediana
            font.bold: true

            Rectangle {
                id: rectCerebroExacto
                width: parent.width*0.6
                height: parent.height/3
                color: "black"
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter

                Text {
                    id: textoCerebroResultadoExacto
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    text: ""
                    font.pixelSize: copaImagen.height*0.3
                    font.bold: true
                }
            }

            BotonImagen {
                id: verOperacionesCerebroExacto

                width: parent.height*0.55
                height: width
                imagePath: "qrc:/images/images/operaciones.png"
                anchors.top: rectCerebroExacto.top
                anchors.left: rectCerebroExacto.right
                anchors.leftMargin: width/5
                onPulsado:  operacionesCerebro.visible = true

                Text {
                    anchors.top: parent.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width
                    text: "¿Cómo?"
                    font.pixelSize: width/4
                    color: "white"
                }
            }
        }

        //FIJO EN TODOS LOS MENSAJES DE FIN DE PARTIDA (imagen y texto reiniciar partida) ----
        BotonImagen
        {
            id: btnReiniciarPartida
            name: "botonReiniciarPartida"
            height: parent.height*0.15
            width: height

            anchors.bottom: txtReiniciarPartida.top
            anchors.horizontalCenter: parent.horizontalCenter
            imagePath: "qrc:/images/images/reiniciarPartida.png"
            onPulsado: reiniciarPartida()
        }

        Text {

            id: txtReiniciarPartida
            text: "Volver a jugar"
            height: parent.height*0.05
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: btnReiniciarPartida.height/8
            color: "white"
            font.pixelSize: parent.height/25
            font.bold: true
        }
    }

    //EXACTO NO ENCONTRADO
    Rectangle {

        id: exactoNoEncontrado
        anchors.centerIn: root
        visible: false
        width: root.width/1.5
        height: root.height/2
        radius: 4
        color: "red"
        border.width: 2
        border.color: "white"

        property string numeroCerebro: textoCerebroResultado.text

        Image {
            id: cronoImagen
            source: "qrc:/images/images/crono.png"
            height: parent.height*0.2
            width: height
            anchors.top: exactoNoEncontrado.top
            anchors.left: exactoNoEncontrado.left

            Text {
                id: textoTiempoAgotado
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.right
                height: parent.height/2
                text: "¡TIEMPO AGOTADO!"
                color: "white"
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                font.pixelSize: cronoImagen.height*0.25
                font.bold: true

            }
        }

        Text {
            id: textoExactoNoEncontrado
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width*0.9
            height: parent.height*0.25
            anchors.top: cronoImagen.bottom
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.Wrap
            text: "Has conseguido el número:"
            color: "white"
            font.pixelSize: tamañoFuenteMediana
            font.bold: true

            Rectangle {
                width: parent.width*0.6
                height: parent.height/3
                color: "black"
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter

                Text {
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    text: valorMasCercano
                    font.pixelSize: cronoImagen.height*0.3
                    font.bold: true
                }
            }
        }

        Text {
            id: centroNoExacto
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width*0.9
            height: parent.height*0.25
            anchors.top: textoExactoNoEncontrado.bottom
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            wrapMode: Text.Wrap
            text: "Cerebro ha conseguido:"
            color: "white"
            font.pixelSize: tamañoFuenteMediana
            font.bold: true

            Rectangle {
                id: rectCerebro
                width: parent.width*0.6
                height: parent.height/3
                color: "black"
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter

                Text {
                    id: textoCerebroResultado
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    color: "white"
                    text: ""
                    font.pixelSize: cronoImagen.height*0.3
                    font.bold: true
                }
            }

            BotonImagen {
                id: verOperacionesCerebro

                width: parent.height*0.55
                height: width
                imagePath: "qrc:/images/images/operaciones.png"
                anchors.top: rectCerebro.top
                anchors.left: rectCerebro.right
                anchors.leftMargin: width/5
                onPulsado:  operacionesCerebro.visible = true

                Text {
                    anchors.top: parent.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width
                    text: "¿Cómo?"
                    font.pixelSize: width/4
                    color: "white"
                }
            }
        }


        //FIJO EN TODOS LOS MENSAJES DE FIN DE PARTIDA (imagen y texto reiniciar partida) ----
        BotonImagen
        {
            id: btnReiniciarPartida2
            name: "botonReiniciarPartida"
            height: parent.height*0.15
            width: height

            anchors.bottom: txtReiniciarPartida2.top
            anchors.horizontalCenter: parent.horizontalCenter
            imagePath: "qrc:/images/images/reiniciarPartida.png"
            onPulsado: reiniciarPartida()
        }

        Text {

            id: txtReiniciarPartida2
            text: "Volver a jugar"
            height: parent.height*0.05
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: btnReiniciarPartida2.height/8
            color: "white"
            font.pixelSize: parent.height/25
            font.bold: true
        }
    }

    // Explicacion Operaciones CEREBRO
    Rectangle {

        id: operacionesCerebro
        anchors.fill: exactoNoEncontrado
        visible: false
        color: "white"
        radius: 4

        Text {
            id: linea1
            anchors.top: parent.top
            anchors.topMargin: parent.height*0.05
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            height: parent.height*0.25
            color: "darkblue"
            text: "- <b>123</b> - <br>Cerebro encontró: <br>Tú encontraste: "
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            font.bold: false
            wrapMode: Text.WordWrap
        }

        GridView {
            id: listaOperacionesCerebro
            anchors.top: linea1.bottom
            anchors.left: parent.left
            height: parent.height *0.5
            width: parent.width
            cellHeight: height / 5
            cellWidth: parent.width

            delegate:
                Rectangle {
                    width: operacionesCerebro.width
                    radius: 4
                    Text {
                        anchors.fill: parent
                        text: modelData
                        color: "black"
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                    }
                }
        }

        BotonImagen {
            id: cerrarOperaciones
            anchors.bottom: parent.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            height: parent.height*0.25
            width: height
            imagePath: "qrc:/images/images/cerrar.png"
            onPulsado: operacionesCerebro.visible = false
        }
    }

 // -- POPUP / Lanzador ---------------- Sobre la pantalla -------------------------

    Popup {

        id: popup
        anchors.fill: root
        modelo: ModeloPopup {}
        onElementoPulsado: {
            console.log("OPERACION PULSADA " + index)
            switch(index)
            {
                case 1: Cifras.reset()
                        cambiarQml("MenuPrincipal.qml")
                        break
                case 2://pausa
                        pausarPartida()
                        break
                case 3: //Ayuda
                        if (sacacifras.enFuncionamiento)
                            empezarEnPausa = true
                        else if(!partidaPausada)
                            pausarPartida()
                        ayuda(1)//es una partida completa
                        break
                case 4: reiniciarPartida()
                        break
                default:
                        break
            }
        }
    }

// -- SLOTS ------------------------------------------------------------------------------

    onCifraSeleccionadaChanged: {
        //desactivamos los botones de la calculadora si no hay cifra seleccionada
        if (cifraSeleccionada)
            calculadora.activo = true
        else
            calculadora.activo = false
    }

    onNumeroDeFichaChanged: {

        if (numeroDeFicha > 5)
        {
            sacacifras.todasSacadas = true
        }
        else
            sacacifras.todasSacadas = false
    }

    onValorMasCercanoChanged: {
        //si el valor es el exacto
        if (valorMasCercano === exacto)
        {
            acabarPartida()
            crono.arrancarPararCrono()
            sobrefondo.visible = true
            setValorLogradoCerebro()
            exactoEncontrado.visible = true
            deseleccionarTodo()
        }
        else
        {
            botonera.colorMasCercano = "green"
            botonera.timerRunning = true
        }
    }

// -- FUNCIONES de SELECCION y DESELECCION ---------------------------------------------------

    function cambiaSeleccion (indice)
    {
        //console.log("cambiaSeleccion() al iniciar INDICE = " + indice)
        //Si hay cifra seleccionada y operacion seleccionada haremos la union de cifras
        if (cifraSeleccionada && operacionSeleccionada)
        {
            //no podemos operar una cifra consigo misma
            if (indice !== indiceCifraSeleccionada)
            {
                unirCifras(indiceCifraSeleccionada,indice,indiceOperacionSeleccionada)
            }
            else
             deseleccionarTodo()
            return
        }

        //Si hay una cifra seleccionada la deseleccionamos para seleccionar la nueva
        if (cifraSeleccionada)
        {
            if (indice === indiceCifraSeleccionada)
            {
                deseleccionarTodo()
                return
            }
            deseleccionarTodo()
            //si la cifra seleccionada es la misma no seleccionamos ninguna mas y salimos
        }

        //seleccionamos la nueva cifra
        seleccionaCifra(indice)
        indiceCifraSeleccionada = indice
        cifraSeleccionada = true
    }

    //Funcion que setea las variables adecuadas para definir que no hay ningun elemento seleccionado
    function ningunaSeleccionada ()
    {
        cifraSeleccionada = false
        indiceCifraSeleccionada = -1
    }

    //Funcion que recorre el listview y deselecciona todas las cifras seleccionadas
    //se usa despues de una union de cifras
    function deseleccionarTodo()
    {
        for (var i=0; i < gridCifras.count; i++)
        {
            gridCifras.currentIndex = i
            gridCifras.currentItem.deseleccionar()
        }

        ningunaSeleccionada()
    }

    //Funcion que recorre el listview y selecciona el indice especificado
    function seleccionaCifra(indice)
    {
        //console.log("seleccionaCifra() INDICE = " + indice)
        //si el indice esta fuera del rango del modelo nos salimos
        if (indice > gridCifras.count)
            return

        gridCifras.currentIndex = indice
        gridCifras.currentItem.seleccionar()
        gridCifras.currentIndex = 0
    }

    //funcion que modifica el modelo para unir 2 huecosCifra despues de una operacion
    function unirCifras(index1, index2, operacion)
    {
        //index1 -> primer indice seleccionado
        //index2 -> segundo indice seleccionado

        //console.log ("Vamos a operar con las cifras con indices: " + index1 + " y " + index2 + " respectivamente")
        var posicion1, posicion2, valor1, valor2
        var nuevoValor
        var valorARestar = 1

        //almacenamos los datos de las cifras con las que vamos a operar
        valor1 = modeloCifras.get(index1).valor
        valor2 = modeloCifras.get(index2).valor

        //console.log ("Vamos a operar con las cifras: " + valor1 + " y " + valor2)
        //Calculamos los nuevos valores
        nuevoValor = Cifras.calcular(valor1,valor2,operacion)
        //console.log ("nuevo valor: " + nuevoValor + " operacion " + operacion)

        //Si es -1 es que el numero no era divisible por el otro
        if (nuevoValor === -1)
        {
            deseleccionarTodo()
            gridCifras.currentIndex = index1
            gridCifras.currentItem.colorTexto = "red"
            gridCifras.currentIndex = index2
            gridCifras.currentItem.colorTexto = "red"
            return
        }

        //comprobamos si es el valor mas cercano al exacto
        compruebaValorMasCercano(nuevoValor)

        //Almacenamos la jugada actual en la lista de jugadas anteriores antes de insertar la nueva
        Cifras.pushJugada(getJugadaActual())

        //Activamos los botones Deshacer y Comenzar
        botonera.activo = true

        //Modificamos el valor de la cifra con indice2 al nuevo valor y el indice1 a 0 para que no se muestre
        modeloCifras.set(index1,{"index": index1, "valor": 0})
        modeloCifras.set(index2,{"index": index2, "valor": nuevoValor})

        //actualizar indices de los elementos del grid ya que hemos eliminado un elemento
        actualizarIndicesGrid()

        //deseleccionamos todas las cifras y selecciamos la cifra resultante de la union
        deseleccionarTodo()
        cambiaSeleccion(index2)

        //pintamos de verde el valor para identificarlo por ser nuevo
        gridCifras.currentIndex = index2
        gridCifras.currentItem.colorTexto = "lightgreen"
    }

// -- Funciones de operaciones con el modelo -------------------------------------

    //Funcion que actualiza el modelo con las cifras iniciales, enviamos los datos a receiver
    function agregarCifra(text)
    {
        modeloCifras.append({"index": numeroDeFicha, "valor": text})
        console.log ("CIFRA "+numeroDeFicha+":  " + text)
        //ENVIAMOS LOS NUMEROS A RECEIVER
        receiver.addCifra(text)
    }

    //devuelve el array con la jugada actual del Grid
    function getJugadaActual()
    {
        var jugada = []
        for (var i=0;i<modeloCifras.count;i++)
            jugada.push(modeloCifras.get(i).valor)

        //console.log("getJugadaActual() = "+jugada)
        return jugada
    }

    //Funcion que actualiza los indices despues de una union de cifras
    function  actualizarIndicesGrid()
    {
        //console.log("actualizarIndicesGrid()")
        for (var i= 0; i< modeloCifras.count; i++)
        {
            //en el modelo
            modeloCifras.get(i).index = i
            //en los objetos
            gridCifras.currentIndex = i
            gridCifras.currentItem.indice = i
        }
    }

    function cargarJugadaEnElModelo(jugada)
    {
        modeloCifras.clear()
        for(var i=0;i<jugada.length;i++)
        {
           modeloCifras.append({"index": i, "valor": jugada[i]})
        }

        actualizarIndicesGrid()
    }

    function cargarJugadaInicial()
    {
        var jugada = Cifras.popJugadaInicial()
        console.log("CARGAR JUGADA INICIAL = "+jugada)
        cargarJugadaEnElModelo(jugada)
        deseleccionarTodo()
    }

    function deshacerJugada()
    {
        var jugada = Cifras.popJugada()
        console.log("CARGAR LA JUGADA = "+ jugada)
        cargarJugadaEnElModelo(jugada)
        deseleccionarTodo()
    }

// ---------------- RESET, PAUSA y FIN de partida  ----------------------------
    function reiniciarPartida()
    {
        //las variables de desarrollo de la partida y botones de pausa
        partidaTerminada = false
        partidaPausada = false
        partidaPausada = false
        botonPlay.visible = false
        popup.cambiarIcono(1,"qrc:/images/images/pause.png")
        popup.cambiarTexto(1,"Pausa")

        //borramos el modelo del grid y deseleccionamos todas las cifras
        modeloCifras.clear()
        deseleccionarTodo()

        //quitamos el sobrefondo por si esta visible y todos los demas elementos
        sobrefondo.visible = false
        exactoEncontrado.visible = false
        exactoNoEncontrado.visible = false

        //reiniciamos el sacacifras y el valor mas cercano
        sacacifras.reset()
        valorMasCercano = 0

        //reiniciamos las propiedades de todos los elementos de cifras e indices
        numeroDeFicha = 0
        indiceCifraSeleccionada = -1 //indice de la cifra seleccionada
        cifraSeleccionada = false
        indiceOperacionSeleccionada = -1
        operacionSeleccionada = false

        //hacemos visibles los elementos ocultados con acabarPartida()
        gridCifras.visible = true
        botonera.visible = true
        botonera.activo = false
        calculadora.visible = true
        operacionesCerebro.visible = false
        linea1.text = ""

        //ponemos el tiempo a cero
        crono.reset()
        intro.restart()
        intro.visible = true

        //Reiniciamos el array de cifras existentes
        receiver.resetCifras()
        Cifras.generaPartida()
    }

    function acabarPartida()
    {
        partidaTerminada = true
        gridCifras.visible = false
        botonera.visible = false
        botonera.activo = false
        calculadora.visible = false
        crono.activo = false
        masTiempo.visible = false
        console.log("===== FIN PARTIDA =====")
    }

    //pausa y da al play a la partida
    function pausarPartida()
    {
        //Si el crono no esta activo no se puede pausar la partida
        //El crono se activa al iniciarse y no se desactiva hasta llega a cero aunque se pare
        if (!crono.activo)
        {
            return
        }

        //si estaba pausada , la reactivamos
        if (partidaPausada)
        {
            partidaPausada = false
            gridCifras.visible = true
            botonPlay.visible = false
            botonera.visible = true
            popup.cambiarIcono(1,"qrc:/images/images/pause.png")
            popup.cambiarTexto(1,"Pausa")
            console.log("===== PARTIDA REANUDADA =====")
        }
        //la pausamos
        else
        {
            partidaPausada = true
            gridCifras.visible = false
            botonera.visible = false
            popup.cambiarIcono(1,"qrc:/images/images/play.png")
            popup.cambiarTexto(1,"Reanudar")
            botonPlay.visible = true
            console.log("===== PARTIDA PAUSADA =====")
        }
        empezarEnPausa = false
        crono.arrancarPararCrono()
    }

    function setValorLogradoCerebro()
    {
        console.log(receiver.valorCerebro)
        textoCerebroResultado.text = receiver.valorCerebro
        textoCerebroResultadoExacto.text = receiver.valorCerebro
        linea1.text = "- <b>" + exacto + "</b> - <br>- "+receiver.listaNumeros+" -<br>Cerebro: <b> "+receiver.valorCerebro+"<br>"
        listaOperacionesCerebro.model = receiver.listaOperaciones
    }

// ----- Modelos de datos ---------------------------------------------

    //Los elementos de todos los modelos son "index" que es el indice que ocupa dentro del modelo
    // y "valor" que es el valor de la cifra que almacena

    //Modelo con las cifras que se van mostrando en pantalla
    ListModel {id: modeloCifras}

//Fin fichero
}
