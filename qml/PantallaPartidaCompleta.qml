import QtQuick 2.0

import "../javascript/PartidaCompleta.js" as Partida
import "../javascript/Records.js" as Records
import "../javascript/Cifras.js" as Cifras
import "../javascript/Letras.js" as Letras

Item {

    id: partidaClasica

    signal cambiarQml (string url)  //SEÑAL COMPARTIDA por todas las pantallas
    signal ayuda (int idPantalla)   //SEÑAL COMPARTIDA por todas las pantalla

    Rectangle {
        id: fondo
        anchors.fill: parent
        color: "#333333"
    }

    Loader {
        id: loader
        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: parent.top
        anchors.bottom: separadorSuperior.top
        source: "PartidaRondaCifras.qml"
    }

    Connections {
        target: loader.item
        ignoreUnknownSignals: true
        onCambiarQml:
        {
            //ponemos todo a cero antes de salir incluido la partida
            Partida.resetPartida()
            reiniciarCifrasYLetras()
            //cambiamos el qml del Loader que hay en main.qml, no este
            cambiarQml(url)
        }
        onContinuar:
        {
                reiniciarCifrasYLetras()
                cargarSiguientePantalla(puntuacion)
        }
        onReiniciarPartidaCompleta:
        {
            reiniciarPartida()
        }
        onAyuda:
        {
            ayuda(idPantalla)
        }
    }

    Rectangle {
        id: separadorSuperior
        height: partidaClasica.height*0.01
        width: partidaClasica.width
        anchors.bottom: row.top
        anchors.horizontalCenter: partidaClasica.horizontalCenter
        color: "black"
    }

    Row {

        id: row
        anchors.bottom: separadorInferior.top
        anchors.left: partidaClasica.left
        height: partidaClasica.height*0.06
        width: partidaClasica.width

        RectRondas { id: ronda1;  ronda: 1;  juego: "C"; puntuacion: -1; activo: true; Component.onCompleted: pintar()}
        RectRondas { id: ronda2;  ronda: 2;  juego: "L"; puntuacion: -1}
        RectRondas { id: ronda3;  ronda: 3;  juego: "L"; puntuacion: -1}
        RectRondas { id: ronda4;  ronda: 4;  juego: "C"; puntuacion: -1}
        RectRondas { id: ronda5;  ronda: 5;  juego: "L"; puntuacion: -1}
        RectRondas { id: ronda6;  ronda: 6;  juego: "L"; puntuacion: -1}
        RectRondas { id: ronda7;  ronda: 7;  juego: "C"; puntuacion: -1}
        RectRondas { id: ronda8;  ronda: 8;  juego: "L"; puntuacion: -1}
        RectRondas { id: ronda9;  ronda: 9;  juego: "L"; puntuacion: -1}
        RectRondas { id: ronda10; ronda: 10; juego: "C"; puntuacion: -1}
    }

    Rectangle {
        id: separadorInferior
        height: partidaClasica.height*0.01
        width: partidaClasica.width
        anchors.bottom: partidaClasica.bottom
        anchors.horizontalCenter: partidaClasica.horizontalCenter
        color: "black"
    }
    // ---- Pantalla Fin de Partida

    FinPartidaCompleta {
        id: finPartida
        anchors.fill: parent
        anchors.centerIn: parent
        onHome:{ Partida.resetPartida(); cambiarQml("MenuPrincipal.qml")}
        onReiniciarPartidaCompleta: {
            reiniciarPartida()
        }
    }

//----- FUNCIONES ----------------------------------------------------------

    //Cargamos la siguiente pantalla salvo que sea la ultima ronda
    function cargarSiguientePantalla(puntuacion)
    {
        //Primero actualizamos el marcador y luego finalizamos la ronda
        actualizarMarcador(puntuacion)
        Partida.finalizarRonda(puntuacion)

        //Si se han acabado las 10 rondas
        if (Partida.rondaActual>10)
        {
            finalizarPartida()
            return
        }

        //Si no se han acabado, comprobamos que toca ahora
        if (Partida.queTocaAhora() === 'C')
        {
            loader.source = "PartidaRondaCifras.qml"
        }
        else
        {
            loader.source = ""
            loader.source = "PartidaRondaLetras.qml"
        }

        console.log("Entramos en la Ronda " + Partida.rondaActual)
    }

    function actualizarMarcador(puntuacion)
    {
        console.log ("RONDA "+ Partida.rondaActual + ": " + puntuacion + " puntos")
        switch(Partida.rondaActual)
        {
            case 1:
                ronda1.puntuacion = puntuacion
                ronda1.activar();ronda2.activar()
                break
            case 2:
                ronda2.puntuacion = puntuacion
                ronda2.activar();ronda3.activar()
                break
            case 3:
                ronda3.puntuacion = puntuacion
                ronda3.activar();ronda4.activar()
                break
            case 4:
                ronda4.puntuacion = puntuacion
                ronda4.activar();ronda5.activar()
                break
            case 5:
                ronda5.puntuacion = puntuacion
                ronda5.activar();ronda6.activar()
                break
            case 6:
                ronda6.puntuacion = puntuacion
                ronda6.activar();ronda7.activar()
                break
            case 7:
                ronda7.puntuacion = puntuacion
                ronda7.activar();ronda8.activar()
                break
            case 8:
                ronda8.puntuacion = puntuacion
                ronda8.activar();ronda9.activar()
                break
            case 9:
                ronda9.puntuacion = puntuacion
                ronda9.activar();ronda10.activar()
                break
            case 10:
                ronda10.puntuacion = puntuacion
                break
        }
    }

    function finalizarPartida()
    {
        //para poder pintar la pantalla establecemos el puesto logrado
        Records.setPuestoLogrado(Partida.puntuacionTotal)
        console.log("Partida Finalizada")
        finPartida.pintarResultados()
        finPartida.visible = true
    }

    //Reiniciamos las cifras y las letras de Cifras.js, Letras.js y Receiver
    function reiniciarPartida()
    {
        Partida.resetPartida()
        reiniciarCifrasYLetras()
        console.log("Reiniciamos partida completa")
        cambiarQml("FondoNegro.qml")
        cambiarQml("PantallaPartidaCompleta.qml")
    }

    function reiniciarCifrasYLetras()
    {
        Letras.reiniciar()
        Cifras.reset()
        receiver.resetLetras()
        receiver.resetCifras()
        Cifras.generaPartida()
    }
}

