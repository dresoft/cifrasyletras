import QtQuick 2.3
import QtQuick.Controls 1.2

Item {
    id: root

    property string imagePath: img.source
    property string name
    property bool activo: true
    property bool noCambiarOpacidad: false
    signal pulsado

    Image
    {
        id: img
        anchors.fill: parent
        source: root.imagePath
        opacity:  (!activo || (mouseArea.containsMouse && activo && !noCambiarOpacidad)) ? 0.5 : 1.0
    }

    MouseArea {

        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        onClicked: {
            //console.log("ImageButton pulsado: " + name + " - activo = " + activo);
            root.pulsado();
            img.opacity = 1.0
        }
    }

    function cambiarImagen(ruta)
    {
        imagePath = ruta
    }
}
