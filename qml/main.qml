import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Window 2.2

import "../javascript/Records.js" as Records
import "../javascript/PartidaCompleta.js" as Partida
import "../javascript/Cifras.js" as Cifras


ApplicationWindow {


    visible: true
    title: qsTr("Cifras y Letras")

    id: main
    objectName: "main"
    property bool autenticado: false

//16:9 - Para pruebas LG - G3
    property int anchoPantalla: 405
    property int altoPantalla: 660
//Screen ajustable - Android
//    property int anchoPantalla: Screen.desktopAvailableWidth
//    property int altoPantalla: Screen.desktopAvailableHeight

    Item {

        id: fondo
        width: main.anchoPantalla
        height: main.altoPantalla
        focus: true

        //Manejamos la tecla retroceder del movil
        Keys.onBackPressed: {
            console.log("Back button captured - Main.qml !")
            console.log("pageLoader.source: "+pageLoader.source)
            //Salimos
            if (pageLoader.source.toString() === "qrc:/qml/MenuPrincipal.qml")
            {
                console.log("Salimos")
                Qt.quit()
            }
            else
            {
                if ((pageLoader.source.toString() === "qrc:/qml/PantallaPartidaCompleta.qml") ||
                    (pageLoader.source.toString() === "qrc:/qml/PantallaCifras.qml") ||
                    (pageLoader.source.toString() === "qrc:/qml/PantallaLetras.qml"))
                {
                    if (!avisoSalir.visible)
                    {
                        avisoSalir.visible = true
                    }
                    else
                    {
                        if (pageLoader.source.toString() === "qrc:/qml/PantallaPartidaCompleta.qml")
                        {
                            console.log("reseteamos partida")
                            Partida.resetPartida()
                        }
                        console.log("Volvemos al menu principal")
                        pageLoader.setSource("MenuPrincipal.qml")
                        temp.stop()
                        avisoSalir.visible = false
                    }
                }
                else
                {
                    console.log("Volvemos al menu principal")
                    pageLoader.setSource("MenuPrincipal.qml")
                }
            }
            event.accepter = true
        }

        Loader {
            id: pageLoader
            width: fondo.width
            height: fondo.height
            anchors.centerIn: fondo
            source: "MenuPrincipal.qml"
        }

        Connections {
            target: pageLoader.item
            ignoreUnknownSignals: true
            onCambiarQml:
            {
                console.log("== Cargamos " + url + " ==")
                receiver.resetCifras()
                receiver.resetLetras()
                Cifras.generaPartida()
//                Cifras.setPartida("999999999999999")
                pageLoader.setSource(url)
            }
            onAyuda: {
                ayuda.setPantalla(idPantalla)
                ayuda.visible = true
            }
        }

        Ayuda {
            id: ayuda
            anchors.fill: parent
            visible: false
        }
    }

// Por ahora no usamos el desplegable lateral

//    Navegador {
//        id: navegador
//        anchors.verticalCenter: fondo.verticalCenter
//        anchors.left: fondo.left
//        onDesplegado: { fondo.width = main.width - pixelesAncho; console.log(fondo.width)}
//        onEscondido: {fondo.width = main.width ; console.log(fondo.width)}
//    }

    Rectangle {
        id: avisoSalir
        anchors.centerIn: parent
        height: parent.height*0.10
        width: parent.width*0.45
        visible: false
        opacity: 0.8
        radius: 6
        color: "white"
        border.width: 2
        border.color: "black"
        onVisibleChanged: {
            if (!visible)
            {
                temp.stop()
                temp.running = false
            }
            else
            {
                temp.start()
            }
        }

        Text {
            id: textoAvisoSalir
            anchors.fill: parent
            wrapMode: Text.WordWrap
            font.pixelSize: height*0.25
            color: "black"
            text: "Pulsa de nuevo retroceder para salir"
            font.family: "Ubuntu"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
        }

        Timer {
            id: temp
            running: false
            interval: 1500
            onTriggered: avisoSalir.visible = false
        }
    }

    Component.onCompleted: {
        console.log("SCREEN WIDTH = " + Screen.desktopAvailableWidth);
        console.log("SCREEN HEIGHT = " + Screen.desktopAvailableHeight);
        setRecords()
//        textoAvisoSalir.visible = true
    }

    function setRecords()
    {
        Records.cargarPuntuacionOro(receiver.puntosOro,receiver.nombreOro)
        Records.cargarPuntuacionPlata(receiver.puntosPlata,receiver.nombrePlata)
        Records.cargarPuntuacionBronce(receiver.puntosBronce,receiver.nombreBronce)
    }

    onAutenticadoChanged: {

                          }
}
