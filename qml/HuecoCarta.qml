import QtQuick 2.3
import QtQuick.Controls 1.2

Rectangle {

    id: root
    width: 62
    height: 100

    border.width: 2
    border.color: "black"
    state: "vacio"

    gradient: Gradient {
             GradientStop { position: 0.0; color: "white" }
             GradientStop { position: 0.5; color: "lightgray" }
             GradientStop { position: 0.8; color: "darkgray" }
    }

    states:

        State {
            name: "vacio"
            PropertyChanges {
                target: root
                color: "lightgray"
            }
        }
        State {
            name: "ocupado"
            PropertyChanges {
                target: root
                color: "black"
            }
        }

}
