import QtQuick 2.0

Item {

    id: root
    opacity: (mouseArea.pressed) ? 0.7 : 1

    property string rutaImagen
    property string texto
    property int  numOperacion

    property int anchoIconos: width*0.5
    property int margen: width*0.05

    signal operacion(int numero)

    Rectangle {
        id: fondo
        width: parent.width*0.90
        height: parent.height*0.90
        anchors.centerIn: parent
        radius: 4
        color: "lightgray"
        opacity: mouseArea.containsMouse ? 0.7 : 1
    }

    Image
    {
        id: imagen
        source: rutaImagen
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: txt.top
        anchors.bottomMargin: height*0.05
        width: anchoIconos
        height: anchoIconos
        opacity: mouseArea.containsMouse ? 0.7 : 1
    }

    Text {
        id: txt
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: fondo.bottom
        anchors.bottomMargin: margen
        horizontalAlignment: Text.AlignHCenter
        width: parent.width
        color: "#000000"
        font.pixelSize: (text.length > 15) ? parent.width/(7+text.length - 15) : parent.width/8
        font.bold: true
        wrapMode: Text.Wrap
        text: root.texto
    }

    MouseArea {
        id: mouseArea
        hoverEnabled: true
        anchors.fill: fondo
        onClicked: { operacion(numOperacion);  cambiaEstado();}
    }

//    onRutaImagenChanged: { imagen.source = rutaImagen; console.log("CAMBIAMOS LA IMAGEN")}
//    onTextoChanged: txt.text = texto
}

