import QtQuick 2.0

// Modelo del desplegable de la pantalla de Cifras   -------------------------------------
ListModel {

    ListElement {
        numeroElemento: 1
        fuenteImagen: "qrc:/images/images/home.png"
        textoBoton: "Home"
    }
    ListElement {
        numeroElemento: 2
        fuenteImagen: "qrc:/images/images/pause.png"
        textoBoton: "Pausa"
    }
    ListElement {
        numeroElemento: 3
        fuenteImagen: "qrc:/images/images/ayuda.png"
        textoBoton: "Ayuda"
    }
    ListElement {
        numeroElemento:4
        fuenteImagen: "qrc:/images/images/reiniciarPartida.png"
        textoBoton: "Reiniciar"
    }
}
