import QtQuick 2.3
import QtQuick.Controls 1.2
import "../javascript/Cifras.js" as Cifras

Rectangle {

    id: root
    radius: 4

    property bool activo
    property int pixelesFuente: parent.height/1.3
    signal finalizado (int exacto)
    property int numExacto
    color: "transparent"

    MouseArea {
        anchors.fill: root
        onClicked: {
            //No hacemos nada
            //console.log("Numero aleatorio activo = " + activo);
            //if(activo) lanzarTimers()
        }
    }

    Rectangle {

        id: centenas
        width: root.width/3
        height: root.height
        anchors.left: root.left
        anchors.top: root.top
        color: "transparent"

        Text {
            id: valCentenas
            anchors.centerIn: parent
            text: "0"
            font.pixelSize: pixelesFuente
            color: "white"
        }
    }

    Rectangle {
        id: decenas
        width: root.width/3
        height: root.height
        anchors.left: centenas.right
        anchors.top: root.top
        color: "transparent"

        Text {
            id: valDecenas
            anchors.centerIn: parent
            text: "0"
            font.pixelSize: pixelesFuente
            color: "white"
        }
    }

    Rectangle {
        id: unidades
        width: root.width/3
        height: root.height
        anchors.right: root.right
        anchors.top: root.top
        color: "transparent"

        Text {
            id: valUnidades
            anchors.centerIn: parent
            text: "0"
            font.pixelSize: pixelesFuente
            color: "white"
        }
    }

//Temporizadores para que los numeros empiecen a cambiar

    Timer {
        id: timerCentenas
        interval: 100
        repeat: true
        running: false
        onTriggered: valCentenas.text = numRandom(9)+1;
    }

    Timer {
        id: timerDecenas
        interval: 100
        repeat: true
        running: false
        onTriggered: valDecenas.text = numRandom(10);
    }

    Timer {
        id: timerUnidades
        interval: 100
        repeat: true
        running: false
        onTriggered: valUnidades.text = numRandom(10);
    }

    function numRandom (de){
            return Math.floor(Math.random() * de);
        }

//Temporizadores de parado

    Timer {
        id: tParaUnidades
        running: false
        repeat: false
        interval: 900
        onTriggered: {
            timerUnidades.running = false;
            valUnidades.color = "yellow"
        }
        onRunningChanged: if (!running) valUnidades.text = Cifras.unidades
    }

    Timer {
        id: tParaDecenas
        running: false
        repeat: false
        interval: 1200
        onTriggered: {
            timerDecenas.running = false;
            valDecenas.color = "yellow"
        }
        onRunningChanged: if (!running) valDecenas.text = Cifras.decenas
    }

    Timer {
        id: tParaCentenas
        running: false
        repeat: false
        interval: 1500
        onTriggered:  {
            console.log("paramos centenas valCentenas = "+valCentenas.text)
            timerCentenas.running = false;
            valCentenas.color = "yellow";
//            obtenerExacto();
            console.log("despues de obtenerExacto valCentenas = "+valCentenas.text)

        }
        onRunningChanged: {
            if (!running) valCentenas.text = Cifras.centenas
            obtenerExacto()
        }
    }

    onActivoChanged: {
        if (activo)
            lanzarTimers();
    }

    function lanzarTimers ()
    {
        if (timerCentenas.running == true)
        {
            timerCentenas.running = false;
            tParaCentenas.running = false;
        }
        else
        {
            timerCentenas.running = true;
            tParaCentenas.running = true;
        }

        if (timerDecenas.running == true)
        {
            timerDecenas.running = false;
            tParaDecenas.running = false;
        }
        else
        {
            timerDecenas.running = true;
            tParaDecenas.running = true;
        }

        if (timerUnidades.running == true)
        {
            timerUnidades.running = false;
            tParaUnidades.running = false;
        }
        else
        {
            timerUnidades.running = true;
            tParaUnidades.running = true;
        }
    }

    onNumExactoChanged: {

        console.log("SacaCifras finalizado");
        finalizado(numExacto);
    }

    function obtenerExacto()
    {
        var uds = parseInt(valUnidades.text);
        var dec = parseInt(valDecenas.text);
        var cen = parseInt(valCentenas.text);
        numExacto = cen*100 + dec*10 + uds;
    }

    function reset()
    {
        valUnidades.text = "0"
        valDecenas.text = "0"
        valCentenas.text = "0"
        activo = false
        tParaCentenas.running = false
        tParaDecenas.running = false
        tParaUnidades.running = false
        timerCentenas.running = false
        timerDecenas.running = false
        timerUnidades.running = false
        valUnidades.color = "white"
        valDecenas.color = "white"
        valCentenas.color = "white"
    }
}

