import QtQuick 2.3
import QtQuick.Controls 1.2

//BOTONES de las operaciones:
//deshacer, reiniciar y mezclar

//PULSADA
//Operaciones: "UNDO" "RESTART"

Rectangle {

    id: root
    color: "transparent"

    property bool activo: false

    property string labelMasCercano: ""
    property string colorMasCercano: "gray"
    property bool timerRunning: false

    signal pulsada (string op)
    signal exactoEncontrado()

//-- Boton DESHACER ----- 25% Ancho 100% Alto -----------------------------------------
    Rectangle
    {
        id: botonDeshacer

        width: parent.width/4
        height: parent.height/2
        anchors.left: parent.left
        anchors.top: parent.top
        color: "transparent"

        visible: root.activo

        Image {
            source: "qrc:/images/images/deshacer.png"
            width: parent.height*0.8
            height:width
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            opacity:  (mouseAreaUndo.containsMouse) ? 0.7 : 1.0
        }

        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            height: parent.height*0.2
            width: parent.width
            text: "Deshacer"
            color: "white"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: parent.height/5
        }

        MouseArea
        {
            id: mouseAreaUndo
            anchors.fill: parent
            onClicked: pulsada("UNDO");
        }
    }

//-- Numero mas cercano al exacto ----- 33% Ancho (verticalCenter) 100% Alto -----------------------------
    Rectangle {
        id: masCercano

        height: parent.height/2
        width: parent.width/3
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        color: colorMasCercano
        radius: 4

        Text {
            id: textoMasCercano
            text: root.labelMasCercano
            anchors.centerIn: parent
            font.pixelSize: parent.height/1.5
            color: "white"
            font.bold: true
        }
    }

//-- Boton REINICIAR ----- 25% Ancho 100% Alto ----------------------------------------
    Rectangle
    {
        id: botonReiniciar

        width: parent.width / 4
        height: parent.height/2
        anchors.right: parent.right
        anchors.top: parent.top
        color: "transparent"
        visible: root.activo

        Image {
            source: "qrc:/images/images/reiniciar.png"
            width: parent.height*0.8
            height:width
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            opacity: (mouseAreaRestart.containsMouse) ? 0.7 :  1.0
        }

        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            height: parent.height*0.2
            width: parent.width
            text: "Comenzar"
            color: "white"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pixelSize: parent.height/5
        }

        MouseArea
        {
            id: mouseAreaRestart
            anchors.fill: parent
            onClicked: pulsada("RESTART");
        }
    }

    //temporizador para que el valor mas cercano vuelva a fondo blanco en 500 ms
    Timer {
        id: vuelveABlanco
        interval: 500
        running: timerRunning
        repeat: false
        onTriggered: {
            colorMasCercano = "gray"
            timerRunning = false
        }
    }

}
