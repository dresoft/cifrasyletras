import QtQuick 2.3
import QtQuick.Controls 1.2

ListModel {
    ListElement {
        texto:"Cifras"
        fuenteImagen:"qrc:/images/images/numbers.png"
        qml:"PantallaCifras.qml"
    }
    ListElement {
        texto:"Letras"
        fuenteImagen:"qrc:/images/images/letters.png"
        qml:"PantallaLetras.qml"
    }
    ListElement {
        texto:"Clásica"
        fuenteImagen:"qrc:/images/images/partida.png"
        qml:"PantallaPartidaCompleta.qml"
    }
    ListElement {
        texto:"Récords"
        fuenteImagen:"qrc:/images/images/trofeo.png"
        qml:"PantallaRecords.qml"
    }
}

