import QtQuick 2.3
import QtQuick.Controls 1.2


Item {

    id: root

    property int valorInicialSegundos: 3
    property int valorSegundos: valorInicialSegundos
    property bool activo: true
    signal tiempoAgotado

    Rectangle {
        id: fondoNegro
        anchors.fill: parent
        color: "#333333"

        MouseArea {
            anchors.fill: parent
        }
    }

    Rectangle {
        id: fondoRedondo
        width: root.width/1.5
        height: width
        anchors.centerIn: parent
        color: "gold"
        radius: 800
    }

    Text
    {
        id: texto
        anchors.centerIn: fondoRedondo
        text: valorSegundos
        font.family: "Sawasdee"
        font.bold: true
        font.pixelSize: fondoRedondo.height/1.5
        color: "white"
        //cambiar a fuente parecida a reloj digital
    }

    Timer {
        id: tempSegundos
        interval: 700; running: true; repeat: true;
        onTriggered: {

            //restamos 1 segundo
            if (valorSegundos>0)
                valorSegundos--;

            //Cambiamos el texto de color segun el tiempo que quede
            var porcentaje = getPorcentajeTiempo()
            switch (valorSegundos)
            {
                case 3:
                   fondoRedondo.color = "yellow";
                   break;
                case 2:
                    fondoRedondo.color = "orange";
                    break;
                case 1:
                    fondoRedondo.color = "red";
                    break;
                default:
                    fondoRedondo.color = "gold"
                    break;
            }

            //paramos el crono al llegar a cero
            if (valorSegundos == 0)
            {
                arrancarPararCrono();
                tiempoAgotado();
                activo = false;
            }
        }
    }

    //SLOTS
    function arrancarPararCrono () {

        if (tempSegundos.running == true)
            tempSegundos.running = false;
        else
            tempSegundos.running = true;
    }

    function restart ()
    {
        activo = true;
        tempSegundos.running = false;
        valorSegundos = valorInicialSegundos;
        arrancarPararCrono();
    }

    function getPorcentajeTiempo()
    {
        return Math.floor((valorSegundos/valorInicialSegundos)*100)
    }
}
