import QtQuick 2.3
import QtQuick.Controls 1.2
//import QtGraphicalEffects 1.0

Item {

    id: root

    property string textoBoton
    property string urlImagen
    property string urlQml

    //Definimos el porcentaje de tamaño de iconos y el margen en los 4 lados
    property int anchoIconos: width*0.5
    property int margen: width*0.05

    signal updateLoader(string url)

    Rectangle {
        id: fondo
        width: parent.width*0.90
        height: parent.height*0.90
        anchors.centerIn: parent
        radius: 8
        color: "#FFFFFF"
        opacity: mouseArea.containsMouse ? 0.7 : 1
    }

    Image
    {
        source: urlImagen
        anchors.top: fondo.top
        anchors.topMargin: margen
        anchors.horizontalCenter: fondo.horizontalCenter
        width: anchoIconos
        height: anchoIconos
//        opacity: mouseArea.containsMouse ? 0.7 : 1
    }

    Text {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: fondo.bottom
        anchors.bottomMargin: margen
        horizontalAlignment: Text.AlignHCenter
        width: fondo.width
        text: textoBoton
        font.family: "FreeMono"
        font.bold: true
        color: "#333333"
        font.pixelSize: fondo.width*0.125
        wrapMode: Text.Wrap
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        onClicked: updateLoader(urlQml)
    }

    Component.onCompleted: console.log(texto)
}

